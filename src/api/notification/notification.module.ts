import { Module } from '@nestjs/common';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';
import { PushNotificationService } from '../push-notification/push-notification.service';
import { ApiService } from '../../utils/ApiService';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PushSubscriptionEntity } from '../entities/push-subscription/PushSubscription.entity';
import { UsersService } from '../users/users.service';
import { AppNotificationEntity } from '../entities/notification/AppNotification.entity';
import { NotificationRepository } from '../repositories/NotificationRepository';
import { ProjectEntity } from '../entities/project/Project.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            PushSubscriptionEntity,
            AppNotificationEntity,
            NotificationRepository,
            ProjectEntity,
        ]),
    ],
    controllers: [NotificationController],
    providers: [NotificationService, PushNotificationService, ApiService, UsersService, AppNotificationEntity],
    exports: [NotificationService, PushNotificationService],
})
export class NotificationModule {
}
