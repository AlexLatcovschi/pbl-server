import { Injectable } from '@nestjs/common';
import { ApiService } from '../../utils/ApiService';
import { PushNotificationService } from '../push-notification/push-notification.service';
import { NotificationGetCountUnseenDto } from './dto/NotificationGetCountUnseenDto';
import { NotificationEventEnum } from '../entities/notification/NotificationEventEnum';
import { NotificationPatchSeenDto } from './dto/NotificationPatchSeenDto';
import { AppNotificationEntity } from '../entities/notification/AppNotification.entity';
import { Request } from 'express';
import { getManager } from 'typeorm';
import { UserEntity } from '../entities/user/User.entity';
import { ProjectEntity } from '../entities/project/Project.entity';
import { UsersService } from '../users/users.service';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';

@Injectable()
export class NotificationService {
  constructor(private push: PushNotificationService,
              private readonly apiService: ApiService,
              private readonly userService: UsersService) {
  }

  public async findMany(req: Request, userId: number, seen?: boolean): Promise<AppNotificationEntity[]> {
    return getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(AppNotificationEntity, 'alias');
      qb.leftJoin('alias.recipient', 'recipient')
        .where('recipient.id = :userId', { userId }).addSelect(['recipient.id']);
      qb.leftJoinAndSelect('alias.author', 'author');
      qb.orderBy('alias.createdAt', 'DESC');
      if (seen !== undefined && seen !== null) {
        qb.andWhere('alias.seen = :seen', { seen });
      }
      return qb.getMany();
    });
  }

  public async projectCreated(recipientId: number, event: NotificationEventEnum, adminUser: UserEntity, project?: ProjectEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = adminUser;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.content = 'Project was created!';
      notification.projectId = project.id;
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('Project was created!',
        '',
        notification.recipient,
        'app/project/' + project.slug);
    });
  }

  public async accountCreated(recipientId: number, event: NotificationEventEnum, adminUser: UserEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = adminUser;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.content = 'Welcome to our app!';
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('Welcome to our app!',
        '',
        notification.recipient,
        '');
    });
  }

  public async addedProjectContributor(recipientId: number, event: NotificationEventEnum, project: ProjectEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = project.creator;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.content = 'Welcome in ' + project.name + ' !';
      notification.projectId = project.id;
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('You was added in a project!',
        'Welcome in ' + project.name + ' !',
        notification.recipient,
        'app/project/' + project.slug);
    });
  }

  public async projectEdited(recipientId: number, event: NotificationEventEnum, project: ProjectEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = project.creator;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.content = 'Project ' + project.name + 'was edited!';
      notification.projectId = project.id;
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('Project ' + project.name + 'was edited!',
        '',
        notification.recipient,
        'app/project/' + project.slug);
    });
  }

  public async addContribution(recipientId: number, event: NotificationEventEnum,
                               project: ProjectEntity, author?, contribution?: ContributionEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = author;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.projectId = project.id;
      const contributionCreator = await this.userService.getUserById(recipientId);
      if (contribution.money) {
        notification.content = ' added a new work time in ';
      }
      if (contribution.time) {
        notification.content = ' added a new investment in ';
      }
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('User ' + contributionCreator.firstName + ' ' + contributionCreator.lastName + ' added activity!',
        '',
        notification.recipient,
        'app/project/' + project.slug);
    });
  }

  public async projectInvitation(recipientId: number, event: NotificationEventEnum, project: ProjectEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = project.creator;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.content = 'You are invited to the project ' + project.name + ' !';
      notification.projectId = project.id;
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('You are invited to the project ' + project.name + ' !',
        '',
        notification.recipient,
        'app/project/' + project.slug);
    });
  }

  public async deleteProjectContributor(recipientId: number, event: NotificationEventEnum, project: ProjectEntity): Promise<void> {
    await getManager().transaction(async em => {
      const notification: AppNotificationEntity = new AppNotificationEntity();
      notification.event = event;
      notification.author = project.creator;
      notification.recipient = { id: recipientId } as UserEntity;
      notification.content = 'You was deleted from the project ' + project.name + ' !';
      notification.projectId = project.id;
      await em.save(AppNotificationEntity, notification);
      this.push.pushNotification('You was deleted from the project ' + project.name + ' !',
        '',
        notification.recipient,
        '');
    });
  }

  public async patchSeen(notificationId: number, userId: number, dto: NotificationPatchSeenDto): Promise<void> {
    await getManager().update(
      AppNotificationEntity, { id: notificationId, recipient: { id: userId } }, { seen: dto.seen });
  }

  public async countUnseen(userId: number): Promise<NotificationGetCountUnseenDto> {
    return getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(AppNotificationEntity, 'alias');
      qb.leftJoin('alias.recipient', 'recipient').where('recipient.id = :userId', { userId });
      qb.andWhere('alias.seen is false');
      return new NotificationGetCountUnseenDto(await qb.getCount());
    });
  }

  public async update(id: number): Promise<AppNotificationEntity> {
    return getManager().transaction(async em => {
      const qb = await em.createQueryBuilder(AppNotificationEntity, 'alias');
      qb.where('alias.id = :id', { id });
      const notification = await qb.getOne();
      notification.seen = true;
      await em.save(AppNotificationEntity, notification);
      return notification;
    });
  }
}
