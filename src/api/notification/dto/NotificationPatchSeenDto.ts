import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class NotificationPatchSeenDto {
    @IsNotEmpty()
    @ApiModelProperty()
    public readonly seen: boolean;
}
