import {ApiModelProperty} from '@nestjs/swagger';

export class NotificationGetCountUnseenDto {
    @ApiModelProperty()
    public readonly totalUnseen: number;

    constructor(count?: number) {
        if (count) this.totalUnseen = count;
    }
}
