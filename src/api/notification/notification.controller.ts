import { Body, Controller, Get, Param, Patch, Put, Query, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { NotificationService } from './notification.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { UserEntity } from '../entities/user/User.entity';
import { NotificationPatchSeenDto } from './dto/NotificationPatchSeenDto';
import { PushNotificationService } from '../push-notification/push-notification.service';
import { UsersService } from '../users/users.service';
import { AuthUser } from '../../decorators/user.decorator';
import { AppNotificationEntity } from '../entities/notification/AppNotification.entity';
import { UserJwt } from '../../auth/dto/UserJwt';
import { AUTH_GUARD } from '../../auth/auth.constants';

@ApiBearerAuth()
@ApiUseTags('Notification')
@Controller('/notifications')
@UseGuards(AUTH_GUARD)

export class NotificationController {
    constructor(
      private readonly notificationService: NotificationService,
      private readonly pushNotification: PushNotificationService,
      private readonly userService: UsersService) {
    }

    @Get()
    public async get(@Req() request: Request, @AuthUser() user: UserJwt, @Query('seen') seen: boolean): Promise<AppNotificationEntity[]> {
        const currentUser = await this.userService.getUserById(user.id);
        return await this.notificationService.findMany(request, currentUser.id, seen);
    }

    @Patch('/:id/seen')
    public async patchSeen(@Param('id') notificationId: number,
                           @AuthUser() { id }: UserEntity, @Body() dto: NotificationPatchSeenDto): Promise<void> {
        return this.notificationService.patchSeen(notificationId, id, dto);
    }

    @Put('/:id')
    public async update(@Body() dto: AppNotificationEntity): Promise<AppNotificationEntity> {
        return await this.notificationService.update(dto.id);
    }

    @Get('/count')
    public async count(): Promise<any> {
        return this.notificationService.countUnseen(1);
    }
}
