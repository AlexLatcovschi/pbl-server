import { Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserImageMulterOptions } from '../../config/multer.config';
import { AUTH_GUARD } from '../../auth/auth.constants';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Uploads')
@Controller('upload')
@UseGuards(AUTH_GUARD)
export class UploadController {
  @Post('users/image')
  @UseInterceptors(FileInterceptor('image', UserImageMulterOptions))
  public async uploadUserImage(@UploadedFile() file): Promise<any> {
    return file;
  }
  @Post('projects/image')
  @UseInterceptors(FileInterceptor('image', UserImageMulterOptions))
  public async uploadProjectImage(@UploadedFile() file): Promise<any> {
    return file;
  }
}
