import { ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Delete, Get, Param, Put, Res, UseGuards } from '@nestjs/common';
import { promisify } from 'util';
import { Response } from 'express';
import { FilesService } from './files.service';
import { AUTH_GUARD } from '../../auth/auth.constants';
import { UserJwt } from '../../auth/dto/UserJwt';
import { AuthUser } from '../../decorators/user.decorator';

@ApiUseTags('Files')
@Controller('public')
export class FilesController {
  constructor(private readonly fileService: FilesService) {
  }

  @Get('uploads/users/images/:fileId')
  public async serveUserImages(@Param('fileId') fileId: string, @Res() res: Response): Promise<any> {
    try {
      await promisify(res.sendFile.bind(res))(fileId, { root: 'public/uploads/users/images' });
    } catch (e) {
      return res.status(404).end();
    }
  }

  @UseGuards(AUTH_GUARD)
  @Put('uploads/users/images')
  public async deleteImage(@Body() image: any, @Res() res: Response, @AuthUser() user: UserJwt): Promise<any> {
    try {
      await this.fileService.removeFileIfExists(image.path);
    } catch (e) {
      return res.status(404).end();
    }
  }

  @Get('uploads/projects/images/:fileId')
  public async serveProjectImages(@Param('fileId') fileId: string, @Res() res: Response): Promise<any> {
    try {
      await promisify(res.sendFile.bind(res))(fileId, { root: 'public/uploads/projects/images' });
    } catch (e) {
      return res.status(404).end();
    }
  }

  @Put('uploads/projects/images')
  public async deleteProjectImage(@Body() image: any, @Res() res: Response, @AuthUser() user: UserJwt): Promise<any> {
    try {
      await this.fileService.removeFileIfExists(image.path);
    } catch (e) {
      return res.status(404).end();
    }
  }
}
