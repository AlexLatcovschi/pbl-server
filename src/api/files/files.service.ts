import { HttpService, Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import { createWriteStream, existsSync, mkdirSync } from 'fs';
import { v4 as uuid } from 'uuid';
import { extname } from 'path';

@Injectable()
export class FilesService {
  private readonly log = new Logger(FilesService.name);

  constructor(private readonly httpService: HttpService) {
  }

  public removeFileIfExistsMultiple(paths: string[]): any {
    paths.forEach(path => this.removeFileIfExists(path));
  }

  public removeFileIfExists(path: string): any {
    this.log.debug('Trying to delete local file: ' + path);
    if (fs.existsSync(path)) {
      this.log.debug('Local file exists, removing...');
      return fs.unlinkSync(path);
    } else {
      this.log.debug('Local file not found... skip.');
    }
  }

  public async urlToLocalFile(url: string, destination: string): Promise<string> {
    if (!existsSync(destination)) {
      mkdirSync(destination, { recursive: true });
    }
    const pictureUrl = `${destination}/${uuid()}${extname(url)}`;
    const writer = createWriteStream(pictureUrl);

    const response = await this.httpService.axiosRef({
      url,
      method: 'GET',
      responseType: 'stream',
    });

    response.data.pipe(writer);

    await new Promise((resolve, reject) => {
      writer.on('finish', resolve);
      writer.on('error', reject);
    });
    return pictureUrl.startsWith('.') ? pictureUrl.substring(2) : pictureUrl;
  }

  public updateOrDeleteFile(oldFile: string, newFile: string, multipleSize?: boolean): string {
    if (newFile && oldFile) {
      if (oldFile !== newFile) {
        if (multipleSize) {
          const allImages = this.getAllImages(oldFile);
          this.removeFileIfExistsMultiple(allImages);
        } else {
          this.removeFileIfExists(oldFile);
        }
        return newFile;
      } else {
        return oldFile;
      }
    }
    if (newFile && !oldFile) {
      return newFile;
    }
    if (!newFile && oldFile) {
      if (multipleSize) {
        const allImages = this.getAllImages(oldFile);
        this.removeFileIfExistsMultiple(allImages);
      } else {
        this.removeFileIfExists(oldFile);
      }
      return newFile;
    }
    return null;
  }

  public updateOrDeleteMultiple(oldFiles: any[], newInputFiles: any[], isObject?: boolean): any[] {
    if (oldFiles && newInputFiles) {
      const newFiles = newInputFiles.filter(value => !oldFiles.includes(value));
      const toBeRemovedFiles = oldFiles.filter(value => !newInputFiles.includes(value));
      const commonFiles = newInputFiles.filter(value => oldFiles.includes(value));
      if (isObject) {
        this.removeFileIfExistsMultiple(toBeRemovedFiles.map(value => value.path));
      } else {
        this.removeFileIfExistsMultiple(toBeRemovedFiles);
      }
      return commonFiles.concat(newFiles);
    } else {
      return newInputFiles;
    }
  }

  private getAllImages(imageUrl: string): string[] {
    const extension = extname(imageUrl);
    imageUrl = imageUrl.replace(extension, '');
    const sizes = ['', '-large', '-medium', '-thumbnail'];
    const allImages = [];
    sizes.map(size => {
      allImages.push(imageUrl + size + extension);
    });
    return allImages;
  }
}
