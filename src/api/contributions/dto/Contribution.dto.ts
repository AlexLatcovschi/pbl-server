import { ApiResponseModelProperty } from '@nestjs/swagger';
import { ContributionEntity } from '../../entities/contribution/Contribution.entity';
import { ProjectEntity } from '../../entities/project/Project.entity';
import { ProjectContributorEntity } from '../../entities/project-contributor/ProjectContributor.entity';

export class ContributionDto {
  @ApiResponseModelProperty()
  public time?: number;
  @ApiResponseModelProperty()
  public money?: number;
  @ApiResponseModelProperty()
  public comment?: string;
  @ApiResponseModelProperty()
  public priceHour?: number;
  @ApiResponseModelProperty()
  public createdAt?: Date;
  @ApiResponseModelProperty()
  public project: ProjectEntity;
  @ApiResponseModelProperty()
  public contributor: ProjectContributorEntity;

  constructor(contribution: ContributionEntity, showPrice?: boolean) {
    if (contribution) {
      if (contribution.time) {
        this.time = contribution.time;
      }
      if (contribution.money) {
        this.money = contribution.money;
      }
      if (contribution.comment) {
        this.comment = contribution.comment;
      }
      if (contribution.priceHour && showPrice) {
        this.priceHour = contribution.priceHour;
      }
      if (contribution.project) {
        this.project = contribution.project;
      }
      if (contribution.projectContributor) {
        this.contributor = contribution.projectContributor;
      }
      this.createdAt = contribution.createdAt;
    }
  }
}
