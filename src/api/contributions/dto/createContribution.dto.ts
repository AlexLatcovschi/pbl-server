import { ApiResponseModelProperty } from '@nestjs/swagger';
import { ContributionDto } from './Contribution.dto';

export class CreateContributionDto {
  @ApiResponseModelProperty()
  public contributorId: number;

  @ApiResponseModelProperty()
  public projectId: number;

  @ApiResponseModelProperty()
  public data: ContributionDto;

  constructor(model: any) {
    if (model) {
      this.contributorId = model.contributorId;
      this.projectId = model.projectId;
      this.data = model.data;
    }
  }
}
