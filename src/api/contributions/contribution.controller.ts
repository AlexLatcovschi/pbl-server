import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { OPTIONAL_AUTH_GUARD } from '../../auth/auth.constants';
import { ContributionService } from './contribution.service';
import { CreateContributionDto } from './dto/createContribution.dto';
import { AuthUser } from '../../decorators/user.decorator';
import { UserJwt } from '../../auth/dto/UserJwt';
import { ContributionDto } from './dto/Contribution.dto';
import { ContributionQuery } from '../query/ContributionQuery';

@ApiUseTags('Contributions')
@ApiBearerAuth()
@UseGuards(OPTIONAL_AUTH_GUARD)
@Controller('contributions')
export class ContributionController {
  constructor(
    private readonly  service: ContributionService,
  ) {
  }

  @Post()
  public createContribution(@Body() model: CreateContributionDto, @AuthUser() user: UserJwt): Promise<ContributionDto> {
    const newContribution = this.service.create(model, user).then(async log => {
      await this.service.sentToAllContributors(log, user.id);
      return log;
    });
    return newContribution;
  }

  @Get()
  public getContributions(@AuthUser() user: UserJwt): Promise<ContributionDto[]> {
    const query = new ContributionQuery();
    query.userId = user.id;
    return this.service.findMany();
  }
}
