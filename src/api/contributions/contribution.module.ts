import { Module } from '@nestjs/common';
import { ApiService } from '../../utils/ApiService';
import { ContributionService } from './contribution.service';
import { ContributionController } from './contribution.controller';
import { ContributorsService } from '../contributors/contributors.service';
import {NotificationModule} from '../notification/notification.module';
import {ProjectContributorRepository} from '../repositories/ProjectContributorRepository';

@Module({
  imports: [NotificationModule],
  providers: [ContributionService, ApiService, ContributorsService, ProjectContributorRepository],
  exports: [ContributionService],
  controllers: [ContributionController],

})
export class ContributionModule {
}
