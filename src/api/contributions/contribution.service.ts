import { Injectable } from '@nestjs/common';
import { EntityManager, getManager } from 'typeorm';
import { ApiService } from '../../utils/ApiService';
import { ProjectContributorEntity } from '../entities/project-contributor/ProjectContributor.entity';
import { CreateContributionDto } from './dto/createContribution.dto';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';
import { ProjectEntity } from '../entities/project/Project.entity';
import { ContributionDto } from './dto/Contribution.dto';
import { ContributionQuery } from '../query/ContributionQuery';
import { UserJwt } from '../../auth/dto/UserJwt';
import { ContributorsService } from '../contributors/contributors.service';
import { ContributorQuery } from '../query/contributorQuery';
import { NotificationService } from '../notification/notification.service';
import { NotificationEventEnum } from '../entities/notification/NotificationEventEnum';

@Injectable()
export class ContributionService {
  constructor(
    private readonly apiService: ApiService,
    private readonly contributorService: ContributorsService,
    private readonly notificationService: NotificationService,
  ) {
  }

  public async create(model: CreateContributionDto, user: UserJwt): Promise<ContributionDto> {
    return await getManager().transaction(async entityManager => {
      const query = new ContributorQuery();
      query.userId = user.id;
      query.projectId = model.projectId;
      const contributor = await this.contributorService.getContributor(query);
      if (!model.contributorId) {
        model.contributorId = contributor.id;
      }
      const dto = model.data;
      const contribution = new ContributionEntity();
      if (dto.time) {
        contribution.time = dto.time;
      }
      if (dto.money) {
        contribution.money = dto.money;
      }
      if (dto.comment) {
        contribution.comment = dto.comment;
      }
      const project = { id: model.projectId } as ProjectEntity;
      contribution.projectContributor = { id: contributor.id } as ProjectContributorEntity;
      contribution.project = project;
      contribution.priceHour = contributor.hourPrice;
      return new ContributionDto(contribution);
    });
  }

  public async findMany(query?: ContributionQuery): Promise<ContributionDto[]> {
    return await getManager().transaction(async entityManager => {
      const qb = await this.getBaseContributionQB(entityManager);
      if (query) {
        if (query.userId) {
          qb.leftJoin('contributor.user', 'user');
          qb.andWhere('user.id = :userId', { userId: query.userId });
        }
        if (query.contributorId) {
          qb.andWhere('contributor.id = :contributorId', { contributorId: query.contributorId });
        }
      }
      const data = await this.apiService.setPagination<ContributionEntity>(qb);
      return data.map(value => new ContributionDto(value, true));
    });
  }

  private getBaseContributionQB(entityManager: EntityManager) {
    const qb = entityManager.createQueryBuilder(ContributionEntity, 'contribution');
    qb.leftJoinAndSelect('contribution.projectContributor', 'contributor');
    qb.leftJoinAndSelect('contribution.project', 'project');
    qb.orderBy('contribution.id', 'DESC');
    return qb;
  }

  public async getContributionById(id): Promise<ContributionEntity> {
    return await getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(ContributionEntity, 'alias');
      qb.where('alias.id = :contributionId', { contributionId: id });
      return await this.apiService.getOne(qb);
    });
  }

  public async sentToAllContributors(contribution, author) {
    const getProject = await getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(ProjectEntity, 'alias');
      qb.where('alias.id = :contributionId', { contributionId: contribution.project.id });
      qb.leftJoinAndSelect('alias.team', 'team');
      qb.leftJoinAndSelect('team.user', 'user');
      return await this.apiService.getOne(qb);
    });
    getProject.team.filter(el => {
      if (el.id !== author) {
        this.notificationService.addContribution(el.user.id, NotificationEventEnum.ADD_CONTRIBUTION, getProject, author, contribution);
        return el;
      }
    });
  }
}
