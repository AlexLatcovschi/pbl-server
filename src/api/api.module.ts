import { Module } from '@nestjs/common';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { RolesGuard } from '../auth/role.guard';
import { ValidationPipe } from '../pipes/validation.pipe';
import { UsersModule } from './users/users.module';
import { AuthModule } from '../auth/auth.module';
import { ProfileModule } from './profile/profile.module';
import { UploadModule } from './upload/upload.module';
import { FilesModule } from './files/files.module';
import { ProjectsModule } from './projects/projects.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectContributorEntity } from './entities/project-contributor/ProjectContributor.entity';
import { ContributorsModule } from './contributors/contributors.module';
import { ContributionModule } from './contributions/contribution.module';
import {PushNotificationModule} from './push-notification/push-notification.module';
import {NotificationModule} from './notification/notification.module';
import { PushSubscriptionEntity } from './entities/push-subscription/PushSubscription.entity';

// @ts-ignore
@Module({
  imports: [
    UsersModule,
    AuthModule,
    ProfileModule,
    UploadModule,
    FilesModule,
    ProjectsModule,
    ContributorsModule,
    ContributionModule,
    TypeOrmModule.forFeature([ProjectContributorEntity]),
    TypeOrmModule.forFeature([ProjectContributorEntity, PushSubscriptionEntity]),
    PushNotificationModule,
    NotificationModule,
  ],
  providers: [
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class ApiModule {
}
