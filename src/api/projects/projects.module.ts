import { Module } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ApiService } from '../../utils/ApiService';
import { ProjectsController } from './projects.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectEntity } from '../entities/project/Project.entity';
import { UserEntity } from '../entities/user/User.entity';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { ProjectRepository } from '../repositories/ProjectRepository';
import { ContributorsService } from '../contributors/contributors.service';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';
import {NotificationModule} from '../notification/notification.module';
import { AppNotificationEntity } from '../entities/notification/AppNotification.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProjectContributorRepository,
      ContributionEntity,
      ProjectEntity,
      UserEntity,
      ProjectRepository,
      AppNotificationEntity,
    ]),
    NotificationModule,
  ],
  providers: [ProjectsService, ApiService, ContributorsService],
  exports: [ProjectsService],
  controllers: [ProjectsController],

})
export class ProjectsModule {
}
