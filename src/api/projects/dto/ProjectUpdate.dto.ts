import { ApiModelProperty } from '@nestjs/swagger';

export class ProjectUpdateDto {
  @ApiModelProperty()
  public name: string;

  @ApiModelProperty()
  public slug: string;

  @ApiModelProperty()
  public type: string;

  @ApiModelProperty()
  public imageUrl: string;
}
