import { ApiModelProperty } from '@nestjs/swagger';

export class ProjectRegisterDto {
  @ApiModelProperty()
  public id: number;

  @ApiModelProperty()
  public name: string;

  @ApiModelProperty()
  public type: string;
}
