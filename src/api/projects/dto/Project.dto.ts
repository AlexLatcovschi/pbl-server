import { ApiResponseModelProperty } from '@nestjs/swagger';
import { ProjectEntity } from '../../entities/project/Project.entity';
import { ProjectContributorDto } from '../../contributors/dto/ProjectContributor.dto';

export class ProjectDto {
  @ApiResponseModelProperty()
  public id?: number;
  @ApiResponseModelProperty()
  public name?: string;
  @ApiResponseModelProperty()
  public slug?: string;
  @ApiResponseModelProperty()
  public type?: string;
  @ApiResponseModelProperty()
  public team?: ProjectContributorDto[];
  @ApiResponseModelProperty()
  public imageUrl?: string;

  constructor(project: ProjectEntity) {
    this.id = project.id;
    this.name = project.name;
    this.slug = project.slug;
    this.type = project.type;
    this.team = [];
    if (project.team) {
      project.team.map(contributor => {
        this.team.push(new ProjectContributorDto(contributor));
      });
    }
    this.imageUrl = project.imageUrl;
  }
}
