import { ApiModelProperty } from '@nestjs/swagger';

export class ProjectAddFounderDto {
  @ApiModelProperty()
  public userId: number;
}
