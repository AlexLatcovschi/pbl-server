import { BadRequestException, Injectable } from '@nestjs/common';
import { EntityManager, getManager } from 'typeorm';
import { ApiService } from '../../utils/ApiService';
import { ProjectDto } from './dto/Project.dto';
import { ProjectEntity } from '../entities/project/Project.entity';
import { ProjectRegisterDto } from './dto/ProjectRegister.dto';
import { UserJwt } from '../../auth/dto/UserJwt';
import { sluggifyString } from '../../utils/string.utils';
import { ProjectContributorEntity } from '../entities/project-contributor/ProjectContributor.entity';
import { UserEntity } from '../entities/user/User.entity';
import { ProjectQueryDto } from '../query/projectQueryDto';
import { ProjectUpdateDto } from './dto/ProjectUpdate.dto';
import { ProjectAddFounderDto } from './dto/ProjectAddFounderDto';
import { ContributorsService } from '../contributors/contributors.service';
import { ContributorQuery } from '../query/contributorQuery';
import { NotificationService } from '../notification/notification.service';
import { NotificationEventEnum } from '../entities/notification/NotificationEventEnum';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';

@Injectable()
export class ProjectsService {
  constructor(
    private readonly apiService: ApiService,
    private readonly contributorService: ContributorsService,
    private readonly notificationService: NotificationService,
  ) {
  }

  public async create(dto: ProjectRegisterDto, user: UserJwt): Promise<ProjectEntity> {
    return getManager().transaction(async entityManager => {
      const slug = sluggifyString(dto.name);
      const existingProject = await entityManager
        .createQueryBuilder(ProjectEntity, 'project')
        .where('project.slug = :slug', { slug })
        .getOne();
      if (existingProject) {
        throw new BadRequestException('Project with this name already exists');
      }
      const project = new ProjectEntity();
      project.name = dto.name;
      project.type = dto.type;
      project.slug = slug;
      project.creator = { id: user.id } as UserEntity;
      const contributor = await this.initContributorData(project.creator, true);
      project.team = [contributor];
      await entityManager.save(ProjectEntity, project);
      console.log(project);
      await this.notificationService.projectCreated(project.creator.id, NotificationEventEnum.PROJECT_CREATED, project.creator, project);
      await this.createContribution(contributor, project, entityManager);
      return project;
    });
  }

  public async update(dto: ProjectUpdateDto, user: UserJwt, projectId: number): Promise<ProjectUpdateDto> {
    return getManager().transaction(async entityManager => {
      const project = await this.getProjectIfCanEdit(user.id, projectId);
      if (!project) {
        throw new BadRequestException('You cannot edit this project');
      }
      project.name = dto.name;
      project.type = dto.type;
      project.slug = await sluggifyString(dto.slug);
      project.imageUrl = dto.imageUrl;
      await entityManager.save(ProjectEntity, project);
      project.team.map(async contr => {
        if (contr.position !== 'ADMIN' && contr.user.id !== user.id) {
          await this.notificationService.projectEdited(contr.user.id, NotificationEventEnum.PROJECT_EDITED, project);
        }
      });
      return project;
    });
  }

  public async search(query?: ProjectQueryDto): Promise<ProjectDto[]> {
    return getManager().transaction(async entityManager => {
      const qb = await this.getBaseProjectQB(entityManager);
      if (query && query.authorId) {
        qb.andWhere('user.id = :userId', { userId: query.authorId });
      }
      if (query && query.slug) {
        qb.andWhere('project.slug = :projectSlug', { projectSlug: query.slug });
      }
      if (query && query.id) {
        qb.andWhere('project.id = :projectId', { projectId: query.id });
      }
      if (query && query.contributorId) {
        qb.andWhere('team.id = :contributorId', { contributorId: query.contributorId });
      }
      const data = await this.apiService.setPagination<ProjectEntity>(qb);
      return data.map(value => new ProjectDto(value));
    });
  }

  public async addContributor(dto: ProjectAddFounderDto, user: UserJwt, projectId: number): Promise<any> {
    return getManager().transaction(async entityManager => {
      const project = await this.getProjectIfCanEdit(user.id, projectId, { onlyCreator: true });
      if (!project) {
        throw new BadRequestException('You cannot add founders to this project');
      }
      const addedUser = { id: dto.userId } as UserEntity;
      await this.notificationService.projectInvitation(addedUser.id, NotificationEventEnum.PROJECT_INVITATION, project);
      const contributor = await this.initContributorData(addedUser);
      // Check if contributor don't exists already
      const contributorExists = project.team.filter((contr) => contr.user.id === addedUser.id);
      if (contributorExists.length) {
        throw new BadRequestException('This user is already in your project!');
      }
      project.team.push(contributor);
      await entityManager.save(ProjectEntity, project);
      await this.createContribution(contributor, project, entityManager);
      return project;
    });
  }

  public async deleteContributor(contributorId: number, user: UserJwt, projectId: number): Promise<any> {
    return getManager().transaction(async entityManager => {
      const project = await this.getProjectIfCanEdit(user.id, projectId, { onlyCreator: true });
      if (!project) {
        throw new BadRequestException('You cannot add founders to this project');
      }
      const query = new ContributorQuery();
      query.contributorId = contributorId;
      const removedUser = await this.contributorService.getContributor(query);
      if (!removedUser) {
        throw new BadRequestException('Contributor not found!');
      }
      // Check if contributor is not founder
      if (removedUser.user.id === project.creator.id) {
        throw new BadRequestException('You cannot delete creator of this project!');
      }
      // Check if contributor exists in project
      const contributorExists = await project.team.filter((contr) => contr.id === removedUser.id);
      if (!contributorExists.length) {
        throw new BadRequestException('This contributor doesnt exists in your project!');
      }
      await this.notificationService.deleteProjectContributor(removedUser.id, NotificationEventEnum.DELETE_PROJECT_CONTRIBUTOR, project);
      await entityManager.delete(ProjectContributorEntity, removedUser.id);
      return project;
    });
  }

  private getBaseProjectQB(entityManager: EntityManager) {
    const qb = entityManager.createQueryBuilder(ProjectEntity, 'project');
    qb.leftJoinAndSelect('project.team', 'team');
    qb.leftJoinAndSelect('project.creator', 'creator');
    qb.leftJoinAndSelect('team.user', 'user');
    qb.orderBy('project.id', 'DESC');
    return qb;
  }

  private async getProjectIfCanEdit(creatorId, projectId, settings?): Promise<ProjectEntity> {
    return getManager().transaction(async entityManager => {
      const qb = this.getBaseProjectQB(entityManager);
      qb.andWhere('creator.id = :creatorId', { creatorId });
      if (!settings.onlyCreator) {
        qb.andWhere('project.id = :projectId', { projectId });
      }

      return qb.getOne();
    });
  }

  private async initContributorData(user: UserEntity, isAdmin = false): Promise<ProjectContributorEntity> {
    const contributor = new ProjectContributorEntity();
    contributor.user = user;
    contributor.hourPrice = 1;
    contributor.investedMoney = 1;
    contributor.position = 'Creator';
    contributor.actions = 100;
    contributor.snacks = 1;
    contributor.joined = new Date();
    contributor.workedTime = 0;

    let color = await this.generateColor();
    if (isAdmin) {
      color = '#BF0000';
    }
    contributor.color = color;
    contributor.status = 'active';
    return contributor;
  }

  private async createContribution(contributor: ProjectContributorEntity, project: ProjectEntity, entityManager) {
    const contribution = new ContributionEntity();
    contribution.time = 1;
    contribution.projectContributor = contributor;
    contribution.comment = 'Joined the project';
    contribution.project = project;
    contribution.priceHour = 1;
    await entityManager.save(ContributionEntity, contribution);
  }

  private async generateColor() {
    const array = ['#34A853', '#17375D', '#E56C0A'];
    const random = Math.round(Math.random() * 100);
    return array[random % array.length];
  }
}
