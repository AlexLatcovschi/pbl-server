import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { OPTIONAL_AUTH_GUARD } from '../../auth/auth.constants';
import { ProjectDto } from './dto/Project.dto';
import { ProjectEntity } from '../entities/project/Project.entity';
import { ProjectRegisterDto } from './dto/ProjectRegister.dto';
import { AuthUser } from '../../decorators/user.decorator';
import { UserJwt } from '../../auth/dto/UserJwt';
import { ProjectQueryDto } from '../query/projectQueryDto';
import { ProjectUpdateDto } from './dto/ProjectUpdate.dto';
import { ProjectAddFounderDto } from './dto/ProjectAddFounderDto';
import { ContributorsService } from '../contributors/contributors.service';
import { ProjectContributorDto } from '../contributors/dto/ProjectContributor.dto';
import { ContributorQuery } from '../query/contributorQuery';
import { NotificationService } from '../notification/notification.service';

@ApiUseTags('Projects')
@ApiBearerAuth()
@UseGuards(OPTIONAL_AUTH_GUARD)
@Controller('projects')
export class ProjectsController {
  constructor(
    private readonly  service: ProjectsService,
    private readonly contributorService: ContributorsService,
    private readonly notificationService: NotificationService,
  ) {
  }

  @Post()
  public createProject(
    @Body() project: ProjectRegisterDto,
    @AuthUser() user: UserJwt,
  ): Promise<ProjectEntity> {
    return this.service.create(project, user);
  }

  @Get()
  public findProjects(): Promise<ProjectDto[]> {
    return this.service.search();
  }

  @Get('id/:id')
  public findProjectById(@Param('id') id: number): Promise<ProjectDto[]> {
    const query = new ProjectQueryDto();
    query.id = id;
    return this.service.search(query);
  }

  @Get('slug/:slug')
  public findProjectBySlug(@Param('slug') slug: string): Promise<ProjectDto[]> {
    const query = new ProjectQueryDto();
    query.slug = slug;
    return this.service.search(query);
  }
  @Get(':slug/contributors/:contributorId')
  public getContributor(@Param('contributorId') contributorId: number,
                        @Param('slug') slug: string): Promise<ProjectContributorDto> {
    const query = new ContributorQuery();
    query.contributorId = contributorId;
    query.projectSlug = slug;
    return this.contributorService.getContributor(query);
  }
  @Get(':id/contributors/')
  public getContributors(@Param('id') projectId: number): Promise<any> {
    const query = new ContributorQuery();
    query.projectId = projectId;
    return this.contributorService.getContributors(query);
  }

  @Put(':id')
  public updateProject(@Param('id') id: number, @Body() dto: ProjectUpdateDto, @AuthUser() user: UserJwt): Promise<ProjectUpdateDto> {
    return this.service.update(dto, user, id);
  }
  @Put(':id/contributor')
  public addContributor(@Param('id') id: number, @Body() dto: ProjectAddFounderDto, @AuthUser() user: UserJwt): Promise<ProjectUpdateDto> {
    return this.service.addContributor(dto, user, id);
  }
  @Delete(':id/contributor/:contributorId')
  public deleteContributor(
    @Param('id') id: number,
    @Param('contributorId') contributorId: number,
    @AuthUser() user: UserJwt): Promise<ProjectUpdateDto> {
    return this.service.deleteContributor(contributorId, user, id);
  }
}
