import {EntityRepository} from 'typeorm';
import {CustomRepository} from './CustomRepository';
import {ProjectContributorEntity} from '../entities/project-contributor/ProjectContributor.entity';

@EntityRepository(ProjectContributorEntity)
export class ProjectContributorRepository extends CustomRepository<ProjectContributorEntity> {
}
