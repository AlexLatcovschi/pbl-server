import { Between, Equal, FindManyOptions, In, Like, Repository, SelectQueryBuilder } from 'typeorm';
import { Request, Response } from 'express';
import { FindOneOptions } from 'typeorm/browser';

/**
 * Provides base repository with pagination and filtering capabilities
 */
export class CustomRepository<T> extends Repository<T> {

  public findOneCustom(req: Request, findOptions?: FindOneOptions<T>): Promise<T> {
    this.processRelations(req.query.relations, findOptions);
    this.processOrder(req.query.sort, findOptions);
    return this.findOne(findOptions);
  }

  public findWithPaginationRaw(req: Request, options?: FindManyOptions<T>): Promise<[T[], number]> {
    const pagination = this.formatPagination(req);

    const findOptions: FindManyOptions<T> = options || { where: {} };
    findOptions.skip = pagination.offset;
    findOptions.take = pagination.limit;

    this.processOrder(req.query.sort, findOptions);
    this.processRelations(req.query.relations, findOptions);

    for (const key in req.query) {
      /* add here all cases */
      if (req.query.hasOwnProperty(key) && key.indexOf('.') > -1) {
        const lastIndex = key.lastIndexOf('.');
        const queryKey = key.substr(lastIndex).substr(1);
        const fieldKey = key.substr(0, lastIndex);
        switch (queryKey) {
          case 'in':
            // TODO -- fix this
            findOptions.where[fieldKey] = In([+req.query[key]]); // };
            break;
          case 'contains':
            // TODO -- implement case insensitive
            findOptions.where[fieldKey] = Like('%' + req.query[key] + '%');
            break;
          case 'equals':
            this.processEqualsQuery(fieldKey, req.query[key], findOptions);
            break;
          case 'between':
            const dates = req.query[key].split(',');
            if (!dates || !dates.length || dates.length < 2) {
              break;
            }
            findOptions.where[fieldKey] = Between(dates[0], dates[1]);
            break;
          default:
            break;
        }
      }
    }
    return this.findAndCount(findOptions);
  }

  public async findWithPagination(req: Request, res: Response, options?: FindManyOptions<T>): Promise<any> {
    return this.formatPaginatedResponse(res, await this.findWithPaginationRaw(req, options));
  }

  /**
   * TODO - add documentation
   * @param req
   * @param res
   * @param options
   */
  public async findWithPaginationQB(req: Request, res: Response, options: FindManyOptions<T> = {}): Promise<any> {
    const alias = 'alias';
    const pagination = this.formatPagination(req);
    const queryBuilder: SelectQueryBuilder<T> = this.createQueryBuilder(alias);

    if (req.query) {
      this.processRelations(req.query.relations, options);
    }
    this.processOrderQB(req.query.order, queryBuilder);

    queryBuilder.offset(pagination.offset);
    queryBuilder.limit(pagination.limit);

    if (options.relations) {
      for (const relation of options.relations) {
        const [key, value] = relation.split('.');
        if (value) {
          queryBuilder.leftJoinAndSelect(relation, value);
        } else {
          queryBuilder.leftJoinAndSelect(`${alias}.${key}`, key);
        }
      }
    }

    if (options && options.where) {
      this.eachRecursive(options.where, queryBuilder);
    }

    for (const key in req.query) {
      /* add here all cases */
      if (req.query.hasOwnProperty(key) && key.indexOf('.') > -1) {
        const lastIndex = key.lastIndexOf('.');
        const queryKey = key.substr(lastIndex).substr(1);
        const fieldKey = key.substr(0, lastIndex);
        switch (queryKey) {
          case 'in':
            queryBuilder.andWhere(`${fieldKey}.id =:${fieldKey}`, { [fieldKey]: req.query[key] });
            break;
          case 'contains':
            const keys = fieldKey.split(',');
            for (const key1 of keys) {
              queryBuilder.orWhere(`LOWER(alias.${key1}) like LOWER(:${key1})`, { [key1]: '%' + req.query[key] + '%' });
            }
            break;
          case 'equalsJ':
            if (this.aliasNotAlreadyDefined(queryBuilder, `alias.${fieldKey}`)) {
              queryBuilder.innerJoin(`alias.${fieldKey}`, fieldKey);
            }
            queryBuilder.andWhere(`${fieldKey}.id =:${fieldKey}`, { [fieldKey]: req.query[key] });
            break;
          case 'equals':
            queryBuilder.andWhere(`alias.${fieldKey} =:${fieldKey}`, { [fieldKey]: req.query[key] });
            break;
          default:
            break;
        }
      }
    }

    return this.formatPaginatedResponse(res, await queryBuilder.getManyAndCount());
  }

  public formatPaginatedResponse(res: Response, data: [T[] | any[], number]): any {
    res.append('X-Total-Count', String(data[1]));
    return res.send(data[0]);
  }

  private eachRecursive(obj: any, queryBuilder: SelectQueryBuilder<T>, lastAlias?: string): void {
    for (const k in obj) {
      if (obj.hasOwnProperty(k)) {
        if (typeof obj[k] === 'object' && obj[k] !== null) {
          const alias = `${lastAlias || 'alias'}.${k}`;
          if (this.aliasNotAlreadyDefined(queryBuilder, alias)) {
            queryBuilder.innerJoin(alias, k);
          }
          this.eachRecursive(obj[k], queryBuilder, k);
        } else {
          queryBuilder.where(`${lastAlias}.${k} =:${k}`, { [k]: obj[k] });
        }
      }
    }
  }

  private aliasNotAlreadyDefined(queryBuilder: SelectQueryBuilder<T>, alias: string): boolean {
    return !queryBuilder.expressionMap.joinAttributes.find(value => value.entityOrProperty === alias);
  }

  private formatPagination(req: Request): { offset: number, limit: number } {
    const limit = req.query.size || 20;
    return { offset: limit * (req.query.page || 0), limit };
  }

  /**
   * If "key.equals" is present in "req.query" apply this filter;
   * @param key - where[key], equals condition key
   * @param value - equals condition value
   * @param findOptions
   */
  private processEqualsQuery(key: string, value: any, findOptions: FindManyOptions<T>): void {
    if (value && key && this.entityHasOwnProperty(key)) {
      value = isNaN(value) ? value : +value;
      // FIXME --- add where subKey here or check developer test-cases
      findOptions.where[key] = Equal(value);
    }
  }

  /**
   * If "relations" field is present in "req.query" load entity relations;
   * Example: "relations=roles,companies" query string will load entity with "roles" & "companies"
   * @param relationsObject
   * @param findOptions
   */
  private processRelations(relationsObject: string, findOptions: any): void {
    if (!relationsObject) {
      return;
    }
    findOptions.relations = [];
    const objectRelationsArray = relationsObject.split(',');
    for (const k of objectRelationsArray) {
      if (this.entityHasOwnProperty(k)) {
        findOptions.relations.push(k);
      }
    }
  }

  /**
   * If desired order field ends with "-", order descending, otherwise order ascending.
   * Example: order=firstName-,id => order by "firstName" descending and "id" ascending.
   * @param orderObject
   * @param findOptions
   */
  private processOrder(orderObject: string, findOptions: FindManyOptions<T>): void {
    if (!orderObject || typeof orderObject !== 'string') {
      return;
    }
    findOptions.order = {};
    const orderObjectArray = orderObject.split(',');
    for (const k of orderObjectArray) {
      if (k.endsWith('-')) {
        if (this.entityHasOwnProperty(k.slice(0, -1))) {
          findOptions.order[k.slice(0, -1)] = 'DESC';
        }
      } else {
        if (this.entityHasOwnProperty(k)) {
          findOptions.order[k] = 'ASC';
        }
      }
    }

  }

  private processOrderQB(orderObject: string, queryBuilder: SelectQueryBuilder<T>): void {
    if (!orderObject) {
      return;
    }
    const orderObjectArray = orderObject.split(',');
    for (const k of orderObjectArray) {
      if (k.endsWith('-')) {
        if (this.entityHasOwnProperty(k.slice(0, -1))) {
          queryBuilder.orderBy(`alias.${k.slice(0, -1)}`, 'DESC');
        }
      } else {
        if (this.entityHasOwnProperty(k)) {
          queryBuilder.orderBy(`alias.${k}`, 'DESC');
        }
      }
    }

  }

  /**
   * Check if Model:T has property
   * @param field
   */
  private entityHasOwnProperty(field: string): boolean {
    return this.metadata.propertiesMap.hasOwnProperty(field.split('.')[0]);
  }
}
