import { EntityRepository } from 'typeorm';
import { CustomRepository } from './CustomRepository';
import { AppNotificationEntity } from '../entities/notification/AppNotification.entity';

@EntityRepository(AppNotificationEntity)
export class NotificationRepository extends CustomRepository<AppNotificationEntity> {
}
