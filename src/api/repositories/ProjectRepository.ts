import { EntityRepository } from 'typeorm';
import { CustomRepository } from './CustomRepository';
import { ProjectEntity } from '../entities/project/Project.entity';

@EntityRepository(ProjectEntity)
export class ProjectRepository extends CustomRepository<ProjectEntity> {
}
