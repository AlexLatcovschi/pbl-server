import {EntityRepository} from 'typeorm';
import {CustomRepository} from './CustomRepository';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';

@EntityRepository(ContributionEntity)
export class ContributionRepository extends CustomRepository<ContributionEntity> {
}
