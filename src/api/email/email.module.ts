import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import * as sgMail from '@sendgrid/mail';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';

@Module({
  imports: [ConfigModule],
  controllers: [],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {
  constructor(private readonly config: ConfigService) {
    sgMail.setApiKey(this.config.get('SENDGRID_API_KEY'));
  }
}
