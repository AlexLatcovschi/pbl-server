import { Injectable, Logger } from '@nestjs/common';
import * as sgMail from '@sendgrid/mail';
import { UserEntity } from '../entities/user/User.entity';
import { ConfigService } from '../../config/config.service';

@Injectable()
export class EmailService {
  public endMessage: string;
  private readonly log = new Logger(EmailService.name);

  constructor(private readonly config: ConfigService) {
    this.endMessage = `With respect, team of <a style="color: #6554C0;" href="https://divysion.com">${this.config.get(
      'APP_NAME',
    )}</a>`;
  }

  public sendActivationEmail(user: UserEntity): Promise<any> {
    const activationUrl = `${this.config.get('CLIENT_FULL_HOST')}/account/activate?email=${user.email}&key=${
      user.activationKey
    }`;
    return this.sendEmail(
      user.email,
      `Activare Email ${this.config.get('APP_NAME')}!`,
      `
<head>
Salut  ${user.firstName}, <br>
  Pentru a-ți activa contul pe platforma
   <a style="color: #6554C0;" href="https://campanii.tv8.md">campanii.tv8.md</a>
    te rugăm să accesezi următorul  link: <br>
     <a style="color: #6554C0;" href="${activationUrl}">${activationUrl}</a> <br><br>
     ${this.endMessage}`,
    );
  }

  public sendResetPasswordEmail(user: UserEntity): Promise<any> {
    const activationUrl = `${this.config.get('CLIENT_FULL_HOST')}/account-recovery/finish?email=${user.email}&key=${
      user.resetKey
    }`;
    return this.sendEmail(
      user.email,
      `${this.config.get('APP_NAME')}, resetarea parolei!`,
      `Hello ${user.firstName}, <br>
                    Pentru a vă reseta parola, vă rugăm să accesați acest link
                    <a href="${activationUrl}">${activationUrl}</a> <br><br>
                   ${this.endMessage}
                    `,
    );
  }

  public sendResetPasswordConfirmation(user: UserEntity): Promise<any> {
    return this.sendEmail(
      user.email,
      `${this.config.get('APP_NAME')}, parola a fost schimbată!`,
      `Salut ${user.firstName}, <br>
                    Parola dumnevoastră a fost resetată.<br>
                    <br><br>
                  ${this.endMessage}
                    `,
    );
  }

  public sendEmail(to: string, subject: string, body: string, from: string = 'office@sens.media'): Promise<any> {
    if (!to) {
      this.log.warn('Receiver email not specified, skip email sending...');
      return;
    }
    if (process.env.NODE_ENV === 'development') {
      this.log.debug(process.env.NODE_ENV + ' skip email sending...');
      return;
    }
    const msg: any = {
      to,
      from: { name: this.config.get('APP_NAME'), email: from },
      subject,
      html: body,
    };
    return sgMail.send(msg);
  }
}
