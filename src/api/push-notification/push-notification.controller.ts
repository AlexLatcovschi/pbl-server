import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { OPTIONAL_AUTH_GUARD } from '../../auth/auth.constants';
import { getManager } from 'typeorm';
import {AuthUser} from '../../decorators/user.decorator';
import {PushSubscriptionEntity} from '../entities/push-subscription/PushSubscription.entity';

@ApiUseTags('Firebase')
@ApiBearerAuth()
@UseGuards(OPTIONAL_AUTH_GUARD)
@Controller('/push-notifications')
export class PushNotificationController {

  @Post()
  public subscribe(@AuthUser() user: any, @Body() pushSubscription: PushSubscriptionEntity & { id: number }): Promise<any> {
    return getManager().transaction(async entityManager => {
      const existent = await entityManager.findOne(PushSubscriptionEntity, { where: { user, endpoint: pushSubscription.endpoint } });
      if (existent) {
        existent.keys = pushSubscription.keys;
        pushSubscription.id = existent.id;

      }
      pushSubscription.user = user;
      return entityManager.save(PushSubscriptionEntity, pushSubscription);
    });
  }
}
