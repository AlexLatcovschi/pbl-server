import { Injectable, Logger } from '@nestjs/common';
import { getManager } from 'typeorm';
import {PushSubscriptionEntity} from '../entities/push-subscription/PushSubscription.entity';
import * as webpush from 'web-push';
import {UserEntity} from '../entities/user/User.entity';
// import { PostEntity } from '../entities/post/PostEntity';

@Injectable()
export class PushNotificationService {

  private readonly logger = new Logger(PushNotificationService.name);

  public pushNotification(title: string, body?: string, recipient?: UserEntity, url?: string): any {
    const notificationPayload = {
      notification: {
        title: { title }.title,
        body: { body }.body,
        icon: 'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiOq-uA3_blAhWCJFAKHX0nDy4QjRx6BAgBEAQ&url=https%3A%2F%2Ffreeiconshop.com%2Ficon%2Fnotification-icon-flat%2F&psig=AOvVaw3IMUt4b_c_KgyXU39bgMw8&ust=1574268927578722',
        vibrate: [100, 50, 100],
        requireInteraction: true,
        data: {
          dateOfArrival: Date.now(),
          primaryKey: 1,
          redirectUrl: url,
        },
        actions: [{
          action: 'explore',
          title: 'Go on site!',
        }],
      },
    };
    if (recipient) {
      return getManager().transaction(async entityManager => {
        const qb = entityManager.createQueryBuilder(PushSubscriptionEntity, 'alias');
        qb.leftJoinAndSelect('alias.user', 'user');

        qb.andWhere('user.id = :userId', { userId: recipient.id });
        const subsctiptions: PushSubscriptionEntity[] = await qb.getMany();
        this.logger.debug('FOUND USER SUBSCRIPTIONS!!! ' + JSON.stringify(subsctiptions));
        for (const subsctiption of subsctiptions) {
          try {
            const re = await webpush.sendNotification(subsctiption, JSON.stringify(notificationPayload));
            console.log('Push Notification sent! ', re);
          } catch (reason) {
            console.error('Push notification error', JSON.stringify(reason));
            if (reason && reason.statusCode && reason.statusCode === 410) {
              const del = await entityManager.remove(PushSubscriptionEntity, subsctiption);
              console.log('Push Notification subscription deleted! ', del, subsctiption);
            }
          }
        }
        return subsctiptions;
      });
    }
  }

/*  public createPostUrl(post: PostEntity): string {
    let url = '';
    if (post.type === 'ARTICLE' || post.type === 'LIBRARY_ARTICLE') {
      url = 'library/' + post.id;
    } else if (post.type === 'POST') {
      url = 'feed/post/' + post.id;
    }
    return url;
  }*/
}
