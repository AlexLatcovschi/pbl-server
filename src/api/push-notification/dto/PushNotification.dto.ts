import { ApiResponseModelProperty } from '@nestjs/swagger';
import { UserEntity } from '../../entities/user/User.entity';

export class PushNotificationDto {
  @ApiResponseModelProperty()
  public id?: number;
  @ApiResponseModelProperty()
  public name?: string;
  @ApiResponseModelProperty()
  public team?: UserEntity[];
  @ApiResponseModelProperty()
  public activity?: string;
  @ApiResponseModelProperty()
  public img?: string;

  constructor(project: PushNotificationDto) {
  }
}
