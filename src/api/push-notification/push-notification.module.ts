import { Module, OnModuleInit } from '@nestjs/common';
import { PushNotificationService } from './push-notification.service';
import * as webpush from 'web-push';
import { PushNotificationController } from './push-notification.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PushSubscriptionEntity } from '../entities/push-subscription/PushSubscription.entity';
import { AppNotificationEntity } from '../entities/notification/AppNotification.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PushSubscriptionEntity,
      AppNotificationEntity,
    ]),
  ],
  controllers: [PushNotificationController],
  providers: [PushNotificationService],
})
export class PushNotificationModule implements OnModuleInit {
  public onModuleInit(): any {
    webpush.setVapidDetails(
      'mailto:equivalent.md',
      process.env.WEB_PUSH_PUBLIC_KEY,
      process.env.WEB_PUSH_PRIVATE_KEY,
    );
  }
}
