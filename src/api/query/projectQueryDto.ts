import { BaseQueryDto } from './baseQueryDto';
import { ApiModelProperty } from '@nestjs/swagger';

export class ProjectQueryDto extends BaseQueryDto {

  @ApiModelProperty()
  public authorId?: number;

  @ApiModelProperty()
  public contributorId?: number;

  @ApiModelProperty()
  public getContributors?: boolean;

  @ApiModelProperty()
  public slug?: string;
}
