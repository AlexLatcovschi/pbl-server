import { ApiModelProperty } from '@nestjs/swagger';
import { BaseQueryDto } from './baseQueryDto';

export class UserQueryDto extends BaseQueryDto {

  @ApiModelProperty()
  public readonly isBlocked?: boolean;

  @ApiModelProperty()
  public readonly search?: string;

  @ApiModelProperty()
  public readonly projectId?: number;
}
