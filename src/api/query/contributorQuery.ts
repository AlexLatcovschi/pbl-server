import { BaseQueryDto } from './baseQueryDto';
import { ApiModelProperty } from '@nestjs/swagger';

export class ContributorQuery extends BaseQueryDto {
  @ApiModelProperty()
  public contributorId?: number;

  @ApiModelProperty()
  public userId?: number;

  @ApiModelProperty()
  public projectId?: number;

  @ApiModelProperty()
  public projectSlug?: string;
}
