import { BaseQueryDto } from './baseQueryDto';
import { ApiModelProperty } from '@nestjs/swagger';

export class ContributionQuery extends BaseQueryDto {

  @ApiModelProperty()
  public userId?: number;

  @ApiModelProperty()
  public projectId?: number;

  @ApiModelProperty()
  public contributorId?: number;

  @ApiModelProperty()
  public projectSlug?: string;
}
