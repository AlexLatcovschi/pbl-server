import { ApiModelProperty } from '@nestjs/swagger';

export class BaseQueryDto {
  @ApiModelProperty()
  public id: number;
}
