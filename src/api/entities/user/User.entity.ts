import { BeforeInsert, Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { PasswordTransformer } from './transformers/password.transformer';
import { AuthProviderEnum } from '../../enum/AuthProvider.enum';
import { RoleEntity } from './role/Role.entity';
import { AbstractEntity } from '../AbstractEntity';
import { UserStatusEnum } from '../../enum/UserStatus.enum';
import { ProjectContributorEntity } from '../project-contributor/ProjectContributor.entity';
import { PushSubscriptionEntity } from '../push-subscription/PushSubscription.entity';

@Entity({ name: 'app_user' })
export class UserEntity extends AbstractEntity {
  @Column({ name: 'first_name' })
  public firstName: string;

  @Column({ name: 'last_name' })
  public lastName: string;

  @Column({ name: 'email', unique: true, length: 50, nullable: true })
  public email: string;

  @Column({ name: 'description', nullable: true })
  public description: string;

  @Column({ name: 'picture_url', nullable: true })
  public pictureUrl: string;

  @Column({ name: 'activated', default: false })
  public activated: boolean;

  @Column({ name: 'activation_key', select: false, nullable: true, length: 20 })
  public activationKey: string;

  @Column({ name: 'reset_key', select: false, nullable: true, length: 20 })
  public resetKey: string;

  @Column('timestamp', { name: 'reset_date', select: false, nullable: true })
  public resetDate: Date;

  @Column({ name: 'password', select: false, nullable: true, transformer: new PasswordTransformer() })
  public password: string;

  @Column({ name: 'third_party_id', nullable: true })
  public thirdPartyId: string;

  @Column({ name: 'status', enum: UserStatusEnum, default: UserStatusEnum.ACTIVE })
  public status: UserStatusEnum;

  @Column({ name: 'provider', type: 'enum', enum: AuthProviderEnum, default: AuthProviderEnum.LOCAL })
  public provider: AuthProviderEnum;

  @ManyToMany(() => RoleEntity, object => object.users)
  @JoinTable({
    name: 'app_user_roles',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'role_id', referencedColumnName: 'id' },
  })
  public roles: RoleEntity[];

  @OneToMany(() => PushSubscriptionEntity, object => object.user)
  public pushSubscriptions?: PushSubscriptionEntity[];

  @OneToMany(() => ProjectContributorEntity, contributor => contributor.user)
  @JoinColumn({ name: 'contributor_id' })
  public contributors?: ProjectContributorEntity[];

  @BeforeInsert()
  public setIsActivated(): void {
    if (!this.provider) {
      this.provider = AuthProviderEnum.LOCAL;
    }
    if (this.provider !== AuthProviderEnum.LOCAL) {
      this.activated = true;
    }
  }
}
