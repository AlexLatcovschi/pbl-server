import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from '../User.entity';
import { AuthorityEntity } from '../authority/Authority.entity';

@Entity({ name: 'app_role' })
export class RoleEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ length: 50, unique: true })
  public name: string;

  @Column({ name: 'description', nullable: true })
  public description?: string;

  @ManyToMany(() => UserEntity, user => user.roles)
  public users?: UserEntity[];

  @ManyToMany(() => AuthorityEntity, object => object.roles)
  @JoinTable({
    name: 'app_role_authorities',
    joinColumn: { name: 'role_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'authority_name', referencedColumnName: 'name' },
  })
  public authorities?: AuthorityEntity[];

  constructor(name?: string, authorities?: AuthorityEntity[]) {
    this.name = name;
    this.authorities = authorities;
  }
}
