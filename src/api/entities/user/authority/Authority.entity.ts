import { Column, Entity, ManyToMany } from 'typeorm';
import { Length } from 'class-validator';
import { RoleEntity } from '../role/Role.entity';

@Entity({ name: 'app_role_authority' })
export class AuthorityEntity {
  @Length(4, 50)
  @Column({ primary: true, length: 50, unique: true })
  public name: string;

  @Column({ name: 'description', nullable: true })
  public description?: string;

  @ManyToMany(() => RoleEntity, object => object.authorities)
  public roles?: RoleEntity[];
}
