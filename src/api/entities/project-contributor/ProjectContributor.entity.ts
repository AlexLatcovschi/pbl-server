import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { AbstractEntity } from '../AbstractEntity';
import { UserEntity } from '../user/User.entity';
import { ProjectEntity } from '../project/Project.entity';
import { ContributionEntity } from '../contribution/Contribution.entity';

@Entity({ name: 'app_project_contributor' })
export class ProjectContributorEntity extends AbstractEntity {
  @ManyToOne(() => UserEntity, object => object.contributors)
  @JoinColumn({ name: 'user_id' })
  public user?: UserEntity;

  @ManyToOne(() => ProjectEntity, object => object.team, {
    nullable: false,
    onDelete: 'CASCADE',
    cascade: ['insert', 'update'],
  })
  @JoinColumn({ name: 'project_id' })
  public project: ProjectEntity;

  @Column({ name: 'hour-price', nullable: true })
  public hourPrice?: number;

  @Column({ name: 'invested-money', nullable: true })
  public investedMoney?: number;

  @Column({ name: 'position', nullable: true })
  public position?: string;

  @Column({ name: 'actions', nullable: true })
  public actions?: number;

  @Column({ name: 'snacks', nullable: true })
  public snacks?: number;

  @Column({ name: 'joined', nullable: true })
  public joined?: Date;

  @Column({ name: 'worked-time', nullable: true })
  public workedTime?: number;

  @Column({ name: 'color', nullable: true })
  public color?: string;

  @Column({ name: 'status', nullable: true })
  public status?: string;

  @OneToMany(() => ContributionEntity, object => object.projectContributor, {
    nullable: false,
    onDelete: 'CASCADE',
    cascade: ['insert', 'update'],
  })
  @JoinColumn({ name: 'project_id' })
  public contributions: ContributionEntity[];
}
