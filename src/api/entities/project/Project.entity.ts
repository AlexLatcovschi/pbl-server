import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { AbstractEntity } from '../AbstractEntity';
import { ProjectContributorEntity } from '../project-contributor/ProjectContributor.entity';
import { UserEntity } from '../user/User.entity';
import { ContributionEntity } from '../contribution/Contribution.entity';

@Entity({ name: 'app_project' })
export class ProjectEntity extends AbstractEntity {

  @Column({ name: 'name' })
  public name: string;

  @Column({ name: 'slug' })
  public slug: string;

  @Column({ name: 'type' })
  public type: string;

  @Column({ name: 'image_url', nullable: true })
  public imageUrl: string;

  @ManyToOne(() => UserEntity, user => user.contributors, { nullable: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'creator_id' })
  public creator: UserEntity;

  @OneToMany(() => ContributionEntity, contribution => contribution.project, { nullable: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'contribution_id' })
  public contributions: ContributionEntity[];

  @OneToMany(() => ProjectContributorEntity, object => object.project, { nullable: true, onDelete: 'CASCADE', cascade: ['insert', 'update'] })
  @JoinColumn({ name: 'team_id' })
  public team: ProjectContributorEntity[];
}
