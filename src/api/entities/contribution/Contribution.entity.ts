import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { AbstractEntity } from '../AbstractEntity';
import { ProjectEntity } from '../project/Project.entity';
import { ProjectContributorEntity } from '../project-contributor/ProjectContributor.entity';

@Entity({ name: 'contribution_entity' })
export class ContributionEntity extends AbstractEntity {

  @Column({ name: 'comment', nullable: true })
  public comment: string;

  @Column({ name: 'time', nullable: true })
  public time?: number;

  @Column({ name: 'money', nullable: true })
  public money?: number;

  @Column({ name: 'price_hour', nullable: true })
  public priceHour?: number;

  @ManyToOne(() => ProjectEntity, project => project.contributions, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'project_id' })
  public project: ProjectEntity;

  @ManyToOne(() => ProjectContributorEntity, contributor => contributor.contributions, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'contributor_id' })
  public projectContributor: ProjectContributorEntity;
}
