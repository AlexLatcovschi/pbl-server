import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { NotificationEventEnum } from './NotificationEventEnum';
import { AbstractEntity } from '../AbstractEntity';
import { UserEntity } from '../user/User.entity';

@Entity({ name: 'app_notification' })
export class AppNotificationEntity extends AbstractEntity {

    @Column('enum', { name: 'event', enum: NotificationEventEnum })
    public event: NotificationEventEnum;

    @Column('text', { name: 'content', nullable: true })
    public content: string;

    @Column('boolean', { name: 'seen', default: false })
    public seen: boolean;

    @Column({ nullable: true})
    public projectId: number;

    @ManyToOne(() => UserEntity, { nullable: false })
    @JoinColumn({ name: 'author_id' })
    public author: UserEntity;

    @ManyToOne(() => UserEntity, { nullable: false })
    @JoinColumn({ name: 'recipient_id' })
    public recipient: UserEntity;

}
