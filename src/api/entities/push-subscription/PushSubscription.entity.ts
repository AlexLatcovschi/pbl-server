import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import {UserEntity} from '../user/User.entity';

@Entity({ name: 'app_user_push_subscription' })
export class PushSubscriptionEntity {

  @PrimaryGeneratedColumn({ name: 'id' })
  public id: number;

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date;

  @Column({ name: 'endpoint'})
  public endpoint: string;

  @Column('json', { name: 'keys' })
  public keys: {
    p256dh: string;
    auth: string;
  };

  @ManyToOne(() => UserEntity, object => object.pushSubscriptions, { onDelete: 'CASCADE', nullable: false })
  @JoinColumn({ name: 'user_id' })
  public user: UserEntity;

}
