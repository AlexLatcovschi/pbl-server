import { Injectable } from '@nestjs/common';
import { EntityManager, getManager } from 'typeorm';
import { ApiService } from '../../utils/ApiService';
import { ProjectContributorEntity } from '../entities/project-contributor/ProjectContributor.entity';
import { ProjectContributorDto } from './dto/ProjectContributor.dto';
import { UpdateProjectContributorDto } from './dto/UpdateProjectContributor.dto';
import { ContributorQuery } from '../query/contributorQuery';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';
import { NotificationEventEnum } from '../entities/notification/NotificationEventEnum';
import { NotificationService } from '../notification/notification.service';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { ProjectEntity } from '../entities/project/Project.entity';
import { MULTIPLIER } from '../../app.constants';
import { ContributionQuery } from '../query/ContributionQuery';
import { ContributionDto } from '../contributions/dto/Contribution.dto';

@Injectable()
export class ContributorsService {
  constructor(
    private readonly apiService: ApiService,
    private readonly notificationService: NotificationService,
    private readonly projectContributorRepository: ProjectContributorRepository,
  ) {
  }

  public async search(query?: ContributorQuery): Promise<ProjectContributorDto[]> {
    return getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(ProjectContributorEntity, 'contributor');
      qb.leftJoinAndSelect('contributor.user', 'user');
      qb.leftJoin('contributor.project', 'project');
      if (query && (query.contributorId || query.id)) {
        qb.andWhere('contributor.id = :contributorId', { contributorId: query.id ? query.id : query.contributorId });
      }
      if (query && query.projectId) {
        qb.andWhere('project.id = :projectId', { projectId: query.projectId });
      }
      qb.orderBy('contributor.id', 'DESC');
      const data = await this.apiService.setPagination<ProjectContributorEntity>(qb);
      return data.map(value => new ProjectContributorDto(value));
    });
  }

  public async getContributor(query: ContributorQuery): Promise<ProjectContributorDto> {
    return await getManager().transaction(async entityManager => {
      const qb = await this.getBaseContributorQB(entityManager);
      // tslint:disable-next-line:no-console
      let project;
      let allSnacks;
      if (query) {
        if (query.contributorId) {
          qb.andWhere('contributor.id = :contributorId', { contributorId: query.contributorId });
        }
        if (query.userId) {
          qb.andWhere('user.id = :userId', { userId: query.userId });
        }
        if (query.projectId) {
          qb.andWhere('project.id = :projectId', { projectId: query.projectId });
          allSnacks = await this.calculateProjectSnacks(query.projectId);
        }
        if (query.projectSlug) {
          qb.andWhere('project.slug = :projectSlug', { projectSlug: query.projectSlug });
          project = await entityManager.findOneOrFail(ProjectEntity, { where: { slug: query.projectSlug } });
          allSnacks = await this.calculateProjectSnacks(project.id);
        }
      }
      const data = await qb.getOne();
      const myContributions = await entityManager.find(ContributionEntity, { where: { projectContributor: { id: query.contributorId } as ProjectContributorEntity } });
      const myData = await this.getAllData(myContributions);
      return new ProjectContributorDto(data, myData, allSnacks);
    });
  }

  public async updateContributor(dto: UpdateProjectContributorDto, contributorId): Promise<ProjectContributorDto> {
    return await getManager().transaction(async entityManager => {
      const contributor = await entityManager.findOneOrFail(ProjectContributorEntity, contributorId);
      contributor.color = dto.color;
      contributor.status = dto.status;
      contributor.hourPrice = dto.hourPrice;
      contributor.position = dto.position;
      await entityManager.save(ProjectContributorEntity, contributor);
      return new ProjectContributorDto(contributor);
    });
  }

  public async getContributors(query: ContributorQuery): Promise<any> {
    return await getManager().transaction(async entityManager => {
      const qb = await this.getBaseContributorQB(entityManager);
      if (query && query.projectId) {
        qb.andWhere('project.id = :projectId', { projectId: query.projectId });
      }
      const allSnacks = await this.calculateProjectSnacks(query.projectId);
      const data = await this.apiService.setPagination<ProjectContributorEntity>(qb);
      const result = [];
      for (const value of data) {
        query.contributorId = value.id;
        const userContributions = await this.finContributions(query);
        const myData = await this.getAllData(userContributions);
        result.push(new ProjectContributorDto(value, myData, allSnacks));
      }
      return result;
    });
  }

  private async calculateProjectSnacks(projectId: number) {
    return await getManager().transaction(async entityManager => {
      let projectContributions;
      let allSnacks;
      projectContributions = await entityManager.find(ContributionEntity, { where: { project: { id: projectId } as ProjectEntity } });
      if (projectContributions && projectContributions.length) {
        let allInvestedMoneyToSnack = 0;
        let allInvestedTimeToSnack = 0;
        projectContributions.forEach(contribution => {
          if (contribution.money) {
            allInvestedMoneyToSnack += contribution.money * MULTIPLIER.MONEY;
          }
          if (contribution.time) {
            allInvestedTimeToSnack += contribution.time * MULTIPLIER.TIME * contribution.priceHour;
          }
        });
        allSnacks = allInvestedTimeToSnack + allInvestedMoneyToSnack;
      }
      return allSnacks;
    });
  }

  private async getAllData(contributions) {
    const data = {
      money: 0,
      time: 0,
    };
    if (contributions && contributions.length) {
      contributions.forEach(contribution => {
        if (contribution.money) {
          data.money += contribution.money;
        }
        if (contribution.time) {
          data.time += contribution.time;
        }
      });
    }
    return data;
  }

  private getBaseContributorQB(entityManager: EntityManager) {
    const qb = entityManager.createQueryBuilder(ProjectContributorEntity, 'contributor');
    qb.leftJoinAndSelect('contributor.user', 'user');
    qb.leftJoinAndSelect('contributor.project', 'project');
    qb.orderBy('contributor.id', 'DESC');
    return qb;
  }

  public async save(contributor: ProjectContributorEntity): Promise<ProjectContributorEntity> {
    return this.projectContributorRepository.save(contributor);
  }

  private async finContributions(query?: ContributionQuery): Promise<ContributionDto[]> {
    return await getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(ContributionEntity, 'contribution');
      qb.leftJoinAndSelect('contribution.projectContributor', 'contributor');
      qb.leftJoinAndSelect('contribution.project', 'project');
      qb.orderBy('contribution.id', 'DESC');
      if (query) {
        if (query.userId) {
          qb.leftJoin('contributor.user', 'user');
          qb.andWhere('user.id = :userId', { userId: query.userId });
        }
        if (query.contributorId) {
          qb.andWhere('contributor.id = :contributorId', { contributorId: query.contributorId });
        }
        if (query.projectId) {
          qb.andWhere('project.id = :projectId', { projectId: query.projectId });
        }
        if (query.projectSlug) {
          qb.andWhere('project.slug = :projectSlug', { projectSlug: query.projectSlug });
        }
      }
      const data = await this.apiService.setPagination<ContributionEntity>(qb);
      return data.map(value => new ContributionDto(value, true));
    });
  }
}
