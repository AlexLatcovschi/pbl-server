import { Body, Controller, Get, Param, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { OPTIONAL_AUTH_GUARD } from '../../auth/auth.constants';
import { ContributorsService } from './contributors.service';
import { ProjectContributorDto } from './dto/ProjectContributor.dto';
import { UpdateProjectContributorDto } from './dto/UpdateProjectContributor.dto';
import { ContributorQuery } from '../query/contributorQuery';
import { ContributionService } from '../contributions/contribution.service';
import { ContributionDto } from '../contributions/dto/Contribution.dto';

@ApiUseTags('Contributors')
@ApiBearerAuth()
@UseGuards(OPTIONAL_AUTH_GUARD)
@Controller('contributors')
export class ContributorsController {
  constructor(
    private readonly  service: ContributorsService,
    private readonly contributionService: ContributionService,
  ) {
  }

  @Get(':contributorId')
  public getContributor(@Param('contributorId') contributorId: number): Promise<ProjectContributorDto> {
    const query = new ContributorQuery();
    query.contributorId = contributorId;
    return this.service.getContributor(query);
  }

  @Get(':contributorId/contributions')
  public getContributions(@Param('contributorId') contributorId: number): Promise<ContributionDto[]> {
    const query = new ContributorQuery();
    query.contributorId = contributorId;
    return this.contributionService.findMany(query);
  }

  @Put(':contributorId')
  public updateContributor(@Param('contributorId') contributorId: number, @Body() dto: UpdateProjectContributorDto): Promise<ProjectContributorDto> {
    return this.service.updateContributor(dto, contributorId);
  }

}
