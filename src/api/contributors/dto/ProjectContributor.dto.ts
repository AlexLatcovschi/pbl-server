import { ApiResponseModelProperty } from '@nestjs/swagger';
import { ProjectEntity } from '../../entities/project/Project.entity';
import { SimpleUserResponseDto } from '../../users/dto/SimpleUserResponse.dto';
import { ProjectContributorEntity } from '../../entities/project-contributor/ProjectContributor.entity';
import { ContributionDto } from '../../contributions/dto/Contribution.dto';
import { MULTIPLIER } from '../../../app.constants';

export class ProjectContributorDto {
  @ApiResponseModelProperty()
  public id?: number;
  @ApiResponseModelProperty()
  public project?: ProjectEntity;
  @ApiResponseModelProperty()
  public user?: SimpleUserResponseDto;
  @ApiResponseModelProperty()
  public hourPrice?: number;
  @ApiResponseModelProperty()
  public investedMoney?: number;
  @ApiResponseModelProperty()
  public position?: string;
  @ApiResponseModelProperty()
  public actions?: number;
  @ApiResponseModelProperty()
  public snacks?: number;
  @ApiResponseModelProperty()
  public joined?: Date;
  @ApiResponseModelProperty()
  public workedTime?: number;
  @ApiResponseModelProperty()
  public color?: string;
  @ApiResponseModelProperty()
  public status?: string;
  @ApiResponseModelProperty()
  public contributions?: ContributionDto[];

  constructor(contributor: ProjectContributorEntity,
              myData?: { money: number, time: number },
              allSnacks?: number) {
    if (contributor) {
      this.id = contributor.id;
      this.project = contributor.project;
      this.user = contributor.user;
      this.hourPrice = contributor.hourPrice;
      this.position = contributor.position;
      this.snacks = contributor.snacks;
      this.joined = contributor.joined;
      this.investedMoney = 0;
      this.workedTime = 0;
      let data;
      if (myData) {
        data = myData;
      } else {
        data = {
          money: 0,
          time: 0,
        };
      }
      this.investedMoney = data.money;
      this.workedTime = data.time;
      this.snacks = this.calculateSnacks(this.investedMoney, this.workedTime);
      const actions = this.calculateActions(this.snacks, allSnacks);
      this.actions = Math.round(actions);
      this.color = contributor.color;
      this.status = contributor.status;
    }
  }

  private calculateSnacks(money, time) {
    const moneyToSnack = money * MULTIPLIER.MONEY;
    const timeToSnack = time * MULTIPLIER.TIME * this.hourPrice;
    return moneyToSnack + timeToSnack;
  }

  private calculateActions(snacks, allSnacks) {
    if (!snacks) {
      return 0;
    }
    let result: number;
    if (allSnacks) {
      result = (snacks / allSnacks) * 100;
    } else {
      result = 0;
    }
    return result;
  }
}
