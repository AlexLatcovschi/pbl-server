import { ApiResponseModelProperty } from '@nestjs/swagger';
import { ProjectContributorEntity } from '../../entities/project-contributor/ProjectContributor.entity';

export class UpdateProjectContributorDto {
  @ApiResponseModelProperty()
  public hourPrice: number;
  @ApiResponseModelProperty()
  public color: string;
  @ApiResponseModelProperty()
  public position: string;
  @ApiResponseModelProperty()
  public status: string;

  constructor(contributor: ProjectContributorEntity) {
    if (contributor) {
      this.hourPrice = contributor.hourPrice;
      this.color = contributor.color;
      this.position = contributor.position;
      this.status = contributor.status;
    }
  }
}
