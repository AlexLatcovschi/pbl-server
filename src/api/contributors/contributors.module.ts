import { Module } from '@nestjs/common';
import { ApiService } from '../../utils/ApiService';
import { ContributorsService } from './contributors.service';
import { ContributorsController } from './contributors.controller';
import { ContributionService } from '../contributions/contribution.service';
import {NotificationModule} from '../notification/notification.module';
import {ProjectContributorRepository} from '../repositories/ProjectContributorRepository';

@Module({
  imports: [NotificationModule],
  providers: [ContributorsService, ApiService, ContributionService, ProjectContributorRepository],
  exports: [ContributorsService],
  controllers: [ContributorsController],

})
export class ContributorsModule {
}
