import { IsNotEmpty, Matches } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ChangePasswordDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @Matches(new RegExp('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$'), {
    message: 'Minimum eight characters, at least one letter and one number',
  })
  public newPassword: string;

  @ApiModelProperty()
  @IsNotEmpty()
  public oldPassword: string;
}
