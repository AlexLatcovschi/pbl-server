import { ApiModelProperty } from '@nestjs/swagger';
import { UserStatusEnum } from '../../enum/UserStatus.enum';

export class UpdateUserDto {
  @ApiModelProperty()
  public id: number;

  @ApiModelProperty()
  public firstName: string;

  @ApiModelProperty()
  public lastName: string;

  @ApiModelProperty()
  public email: string;

  @ApiModelProperty()
  public status: UserStatusEnum;

  @ApiModelProperty()
  public description: string;

  @ApiModelProperty()
  public birthDate: Date;

  @ApiModelProperty()
  public pictureUrl: string;
}
