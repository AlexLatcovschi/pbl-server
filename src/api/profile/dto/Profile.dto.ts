import { UserEntity } from '../../entities/user/User.entity';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserStatusEnum } from '../../enum/UserStatus.enum';

export class ProfileDto {
  @ApiModelProperty()
  public id: number;

  @ApiModelProperty()
  public email: string;

  @ApiModelProperty()
  public firstName: string;

  @ApiModelProperty()
  public lastName: string;

  @ApiModelProperty()
  public description: string;

  @ApiModelProperty()
  public activated: boolean;

  @ApiModelProperty()
  public pictureUrl: string;

  @ApiModelProperty()
  public isReported: boolean;

  @ApiModelProperty()
  public isBlocked: boolean;

  @ApiModelProperty()
  public status: UserStatusEnum;

  @ApiModelProperty()
  public canResentActivation: boolean;

  @ApiModelProperty()
  public hasPassword: boolean;

  @ApiModelProperty()
  public roles: string[];

  constructor(profile: UserEntity) {
    this.id = profile.id;
    this.email = profile.email;
    this.firstName = profile.firstName;
    this.lastName = profile.lastName;
    this.description = profile.description;
    this.activated = profile.activated;
    this.pictureUrl = profile.pictureUrl;
    this.hasPassword = !!profile.password;
    if (profile.roles) {
      this.roles = profile.roles.map(value => value.name);
    }
    this.status = profile.status;
    if (profile.resetDate) {
      const now = new Date();
      const coolDown = (now.getTime() - profile.resetDate.getTime()) / 6000;
      this.canResentActivation = coolDown > 60;
    }
    this.isBlocked = this.status === UserStatusEnum.BLOCKED;
  }
}
