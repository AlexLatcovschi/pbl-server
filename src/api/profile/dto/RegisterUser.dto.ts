import { IsEmail, IsNotEmpty, Matches } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterUserDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsEmail()
  public email: string;

  @ApiModelProperty()
  @Matches(new RegExp('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$'), {
    message: 'Minimum six characters, at least one letter and one number',
  })
  @IsNotEmpty()
  public password: string;

  @ApiModelProperty()
  public firstName: string;

  @ApiModelProperty()
  public lastName: string;
}
