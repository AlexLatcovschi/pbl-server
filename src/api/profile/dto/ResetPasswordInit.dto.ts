import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ResetPasswordInitDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsEmail()
  public email: string;
}
