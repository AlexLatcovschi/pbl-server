import { IsDefined, IsNotEmpty, Matches } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ResetPasswordFinishDto {
  @IsNotEmpty()
  @ApiModelProperty()
  public email: string;

  @IsNotEmpty()
  @ApiModelProperty()
  public key: string;

  @Matches(new RegExp('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$'), {
    message: 'Minimum eight characters, at least one letter and one number',
  })
  @IsDefined()
  @ApiModelProperty()
  public newPassword: string;
}
