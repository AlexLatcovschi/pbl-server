import { UserEntity } from '../../entities/user/User.entity';
import { ApiModelProperty } from '@nestjs/swagger';

export class SearchUserDto {
  @ApiModelProperty()
  public id: number;

  @ApiModelProperty()
  public firstName: string;

  @ApiModelProperty()
  public lastName: string;

  @ApiModelProperty()
  public pictureUrl: string;

  constructor(profile: UserEntity) {
    this.id = profile.id;
    this.firstName = profile.firstName;
    this.lastName = profile.lastName;
    this.pictureUrl = profile.pictureUrl;
  }
}
