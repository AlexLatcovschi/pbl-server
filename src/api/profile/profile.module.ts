import { HttpModule, Module } from '@nestjs/common';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { EmailModule } from '../email/email.module';
import { AuthModule } from '../../auth/auth.module';
import { ConfigModule } from '../../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleEntity } from '../entities/user/role/Role.entity';
import { FilesService } from '../files/files.service';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { ProjectsModule } from '../projects/projects.module';
import { ContributionModule } from '../contributions/contribution.module';

@Module({
  imports: [TypeOrmModule.forFeature([
    RoleEntity,
    ProjectContributorRepository,
  ]), ConfigModule, AuthModule, EmailModule, HttpModule, ProjectsModule, ContributionModule],
  controllers: [ProfileController],
  providers: [ProfileService, FilesService],
})
export class ProfileModule {
}
