import { Body, Controller, Get, HttpCode, Post, Put, UseGuards } from '@nestjs/common';
import { ResetPasswordInitDto } from './dto/ResetPasswordInit.dto';
import { ResetPasswordFinishDto } from './dto/ResetPasswordFinish.dto';
import { ProfileService } from './profile.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { ChangePasswordDto } from './dto/ChangePasswordDto';
import { UpdateUserDto } from './dto/UpdateUserDto';
import { RegisterUserDto } from './dto/RegisterUser.dto';
import { AuthUser } from '../../decorators/user.decorator';
import { ActivateEmailDto } from './dto/ActivateEmail.dto';
import { AuthResponseDto } from '../../auth/dto/AuthTokenDto';
import { UserJwt } from '../../auth/dto/UserJwt';
import { AUTH_GUARD } from '../../auth/auth.constants';
import { UserEntity } from '../entities/user/User.entity';
import { ProjectsService } from '../projects/projects.service';
import { ProjectQueryDto } from '../query/projectQueryDto';
import { ProjectDto } from '../projects/dto/Project.dto';
import { ContributionDto } from '../contributions/dto/Contribution.dto';
import { ContributionQuery } from '../query/ContributionQuery';
import { ContributionService } from '../contributions/contribution.service';

@ApiUseTags('Profile')
@Controller()
export class ProfileController {
  constructor(
    private readonly profileService: ProfileService,
    private readonly projectService: ProjectsService,
    private readonly contributionsService: ContributionService
  ) {
  }

  @Post('register')
  public register(@Body() registerUserDto: RegisterUserDto): Promise<AuthResponseDto> {
    return this.profileService.register(registerUserDto);
  }

  @Post('/resend-email')
  @UseGuards(AUTH_GUARD)
  public resendEmail(@AuthUser() user: UserJwt): Promise<void> {
    return this.profileService.resendEmail(user);
  }

  @Post('/activate-email')
  public activateEmail(@Body() dto: ActivateEmailDto): Promise<AuthResponseDto> {
    return this.profileService.activateEmailByToken(dto.email, dto.key);
  }

  @Post('/profile/reset-password/init')
  @HttpCode(201)
  public async resetPasswordInit(@Body() resetPasswordInitDto: ResetPasswordInitDto): Promise<void> {
    return this.profileService.resetPasswordInit(resetPasswordInitDto.email);
  }

  @Post('/profile/reset-password/finish')
  @HttpCode(201)
  public resetPasswordFinish(@Body() resetPasswordFinishDto: ResetPasswordFinishDto): Promise<AuthResponseDto> {
    return this.profileService.resetPasswordFinish(resetPasswordFinishDto);
  }

  @ApiBearerAuth()
  @UseGuards(AUTH_GUARD)
  @Post('/profile/change-password')
  public async changePassword(@Body() changePasswordDto: ChangePasswordDto, @AuthUser() user: UserJwt): Promise<void> {
    await this.profileService.changePassword(changePasswordDto, user.id);
  }

  @ApiBearerAuth()
  @UseGuards(AUTH_GUARD)
  @Put('/profile')
  @HttpCode(201)
  public async update(@Body() dto: UpdateUserDto, @AuthUser() user: UserJwt): Promise<UserEntity> {
    return await this.profileService.update(user, dto);
  }

  @ApiBearerAuth()
  @UseGuards(AUTH_GUARD)
  @Get('/profile')
  @HttpCode(200)
  public async authorize(@AuthUser() user: UserJwt): Promise<UserEntity> {
    return await this.profileService.me(user.id);
  }

  @ApiBearerAuth()
  @UseGuards(AUTH_GUARD)
  @Get('/profile/projects')
  @HttpCode(200)
  public async getProjects(@AuthUser() user: UserJwt): Promise<ProjectDto[]> {
    const query = new ProjectQueryDto();
    query.authorId = user.id;
    return await this.projectService.search(query);
  }

  @Get('/profile/contributions')
  public getContributions(@AuthUser() user: UserJwt): Promise<ContributionDto[]> {
    const query = new ContributionQuery();
    query.userId = user.id;
    return this.contributionsService.findMany();
  }
}
