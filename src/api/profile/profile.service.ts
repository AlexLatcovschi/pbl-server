import { BadRequestException, Injectable } from '@nestjs/common';
import { ResetPasswordFinishDto } from './dto/ResetPasswordFinish.dto';
import * as bcrypt from 'bcrypt';
import { ChangePasswordDto } from './dto/ChangePasswordDto';
import { UpdateUserDto } from './dto/UpdateUserDto';
import { RegisterUserDto } from './dto/RegisterUser.dto';
import { UserEntity } from '../entities/user/User.entity';
import { EntityManager, getConnection, getManager, Repository } from 'typeorm';
import { generateValidationKey } from '../../utils';
import { ConfigService } from '../../config/config.service';
import { AuthService } from '../../auth/auth.service';
import { ProfileDto } from './dto/Profile.dto';
import { AuthResponseDto } from '../../auth/dto/AuthTokenDto';
import { RoleEntity } from '../entities/user/role/Role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserJwt } from '../../auth/dto/UserJwt';
import { EmailService } from '../email/email.service';
import { FilesService } from '../files/files.service';

@Injectable()
export class ProfileService {
  constructor(
    @InjectRepository(RoleEntity) private readonly roleRepository: Repository<RoleEntity>,
    private readonly emailService: EmailService,
    private readonly config: ConfigService,
    private readonly authService: AuthService,
    private readonly fileService: FilesService,
  ) {
  }

  public async register(dto: RegisterUserDto): Promise<AuthResponseDto> {
    return getManager().transaction(async entityManager => {
      const existingUser = await entityManager
        .createQueryBuilder(UserEntity, 'user')
        .where('user.email = :email', { email: dto.email })
        .getOne();

      if (existingUser) {
        throw new BadRequestException('User with email address already exists');
      }

      const role: RoleEntity =
        (await entityManager.findOne(RoleEntity, { name: 'ROLE_USER' })) ||
        (await entityManager.save(RoleEntity, { name: 'ROLE_USER', id: undefined }));
      const user = new UserEntity();
      user.email = dto.email;
      user.password = dto.password;

      user.firstName = dto.email.substr(0, dto.email.indexOf('@'));
      user.lastName = 'User';
      user.roles = [role];
      user.activationKey = generateValidationKey();
      await entityManager.save(UserEntity, user);
      await this.emailService.sendActivationEmail(user);
      return new AuthResponseDto(this.authService.createToken(user));
    });
  }

  public async activateEmailByToken(email: string, activationKey: string): Promise<AuthResponseDto> {
    const user = await getManager()
      .createQueryBuilder(UserEntity, 'user')
      .addSelect('user.activationKey')
      .where('user.email = :email', { email })
      .getOne();

    if (!user) {
      throw new BadRequestException('User does not exist');
    }
    if (user.activated) {
      throw new BadRequestException('User with email is already activated');
    }

    if (!user.activationKey) {
      throw new BadRequestException('Not requested activation.');
    }
    if (user.activationKey !== activationKey) {
      throw new BadRequestException('Incorrect activation key.');
    }

    user.activated = true;
    user.activationKey = null;
    await getManager().save(UserEntity, user);

    return new AuthResponseDto(this.authService.createToken(user));
  }

  public async resetPasswordInit(email: string): Promise<void> {
    const user = await getManager()
      .createQueryBuilder(UserEntity, 'user')
      .addSelect('user.resetDate')
      .where('user.email = :email', { email })
      .getOne();

    if (!user) {
      throw new BadRequestException('Email does not exist');
    }
    user.resetKey = generateValidationKey();
    user.resetDate = new Date();
    await getManager().save(UserEntity, user);
    await this.emailService.sendResetPasswordEmail(user);
  }

  public async resetPasswordFinish(dto: ResetPasswordFinishDto): Promise<AuthResponseDto> {
    const user = await getManager()
      .createQueryBuilder(UserEntity, 'user')
      .addSelect('user.resetKey')
      .where('user.email = :email', { email: dto.email })
      .getOne();

    if (!user) {
      throw new BadRequestException('User not found.');
    }

    if (!user.resetKey) {
      throw new BadRequestException('Not requested reset.');
    }

    if (user.resetKey !== dto.key) {
      throw new BadRequestException('Incorrect activation key.');
    }

    // if (new Date() > user.resetDate) {
    //   throw new BadRequestException('Reset password key expired.');
    // }

    user.resetKey = null;
    user.resetDate = null;
    user.password = dto.newPassword;
    await getManager().save(UserEntity, user);
    await this.emailService.sendResetPasswordConfirmation(user);

    return new AuthResponseDto(this.authService.createToken(user));
  }

  public async changePassword(dto: ChangePasswordDto, currentUserId: number): Promise<void> {
    const user = await getManager()
      .createQueryBuilder(UserEntity, 'user')
      .addSelect('user.password')
      .where('user.id  = :userId', { userId: currentUserId })
      .getOne();

    if (!user) {
      throw new BadRequestException('Invalid password!');
    }

    if (!dto.oldPassword || !user.password) {
      throw new BadRequestException('Password not found');
    }

    if (!bcrypt.compareSync(dto.oldPassword, user.password)) {
      throw new BadRequestException('You entered wrong old password!');
    }

    user.password = dto.newPassword;
    await getManager().save(UserEntity, user);
  }

  public async update(user: UserJwt, dto: UpdateUserDto): Promise<UserEntity> {
    return getManager().transaction(async entityManager => {
      const userId = user.id;
      const profile = await this.getProfile(entityManager, userId);
      const { pictureUrl } = profile;
      if (profile) {
        profile.firstName = dto.firstName;
        profile.lastName = dto.lastName;
        profile.description = dto.description;
        profile.email = dto.email;
        profile.pictureUrl = await this.fileService.updateOrDeleteFile(pictureUrl, dto.pictureUrl, false);
        return entityManager.save(UserEntity, profile);
      } else {
        throw new BadRequestException('User not found or you are not allowed to edit it!');
      }
    });
  }

  public async me(userId: number): Promise<UserEntity> {
    // tslint:disable-next-line:no-console
    return getManager().transaction(async entityManager => {
      const data = await this.getProfile(entityManager, userId);
      if (!data) {
        throw new BadRequestException(`Profile don't exist`);
      }
      return data;
    });
  }

  public async resendEmail(user: UserJwt): Promise<void> {
    const fullUser = await getManager()
      .createQueryBuilder(UserEntity, 'user')
      .addSelect('user.activationKey')
      .addSelect('user.resetDate')
      .where('user.id  = :userId', { userId: user.id })
      .andWhere('user.activated is false')
      .getOne();
    const dtoUser = new ProfileDto(fullUser);
    if (dtoUser.canResentActivation) {
      const resetDate = new Date();
      await getConnection()
        .createQueryBuilder()
        .update(UserEntity)
        .set({
          resetDate,
        })
        .where('id = :id', { id: fullUser.id })
        .execute();
      await this.emailService.sendActivationEmail(fullUser);
    } else {
      throw new BadRequestException(`You exceed limit of email confirmation per time. Try again in one minute`);
    }
  }

  private getProfile(entityManager: EntityManager, userId: number): Promise<UserEntity> {
    const qb = entityManager.createQueryBuilder(UserEntity, 'alias');
    qb.where('alias.id = :userId', { userId });
    qb.addSelect(['alias.resetDate']);
    qb.leftJoinAndSelect('alias.roles', 'roles');
    qb.leftJoinAndSelect('alias.contributors', 'contributors');
    return qb.getOne();
  }
}
