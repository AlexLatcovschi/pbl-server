import { Module } from '@nestjs/common';
import { ProjectContributorService } from './project-contributor.service';
import { ApiService } from '../../utils/ApiService';
import { ProjectContributorController } from './project-contributor.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { ProjectEntity } from '../entities/project/Project.entity';
import { UserEntity } from '../entities/user/User.entity';
import {NotificationModule} from '../notification/notification.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProjectContributorRepository, ProjectEntity, UserEntity]),
    NotificationModule,
  ],
  providers: [ProjectContributorService, ApiService],
  exports: [ProjectContributorService],
  controllers: [ProjectContributorController],

})
export class ProjectContributorModule {
}
