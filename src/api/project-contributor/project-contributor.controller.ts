import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ProjectContributorService } from './project-contributor.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { OPTIONAL_AUTH_GUARD } from '../../auth/auth.constants';
import { ProjectContributorEntity } from '../entities/project-contributor/ProjectContributor.entity';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { ContributorQuery } from '../query/contributorQuery';
import {ProjectContributorDto} from '../contributors/dto/ProjectContributor.dto';
import { Request } from 'express';
import { AuthUser } from '../../decorators/user.decorator';
import { UserJwt } from '../../auth/dto/UserJwt';

@ApiUseTags('Contributors')
@ApiBearerAuth()
@UseGuards(OPTIONAL_AUTH_GUARD)
@Controller('contributors')
export class ProjectContributorController {
  constructor(
    private readonly  projectContributorService: ProjectContributorService,
    private readonly projectContributorRepository: ProjectContributorRepository,
  ) {
  }

  @Post()
  public create(@Body() contributor: ProjectContributorEntity, @AuthUser() user: UserJwt): Promise<ProjectContributorEntity> {
    return this.projectContributorService.save(contributor, user.id);
  }

  @Get()
  public findContributors(): Promise<ProjectContributorDto[]> {
    return this.projectContributorService.search();
  }

  @Get('/:id')
  public async one(@Param('id') id: number): Promise<ProjectContributorDto[]> {
    const query = new ContributorQuery();
    query.id = id;
    return this.projectContributorService.search(query);
  }
}
