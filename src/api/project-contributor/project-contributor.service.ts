import { Injectable } from '@nestjs/common';
import { getManager } from 'typeorm';
import { ApiService } from '../../utils/ApiService';
import { ProjectContributorEntity } from '../entities/project-contributor/ProjectContributor.entity';
import { ContributorQuery } from '../query/contributorQuery';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { NotificationService } from '../notification/notification.service';
import { NotificationEventEnum } from '../entities/notification/NotificationEventEnum';
import { ProjectContributorDto } from '../contributors/dto/ProjectContributor.dto';

@Injectable()
export class ProjectContributorService {
  constructor(
    private readonly apiService: ApiService,
    private readonly projectContributorRepository: ProjectContributorRepository,
    private readonly notificationService: NotificationService,
  ) {
  }

  public async search(query?: ContributorQuery): Promise<ProjectContributorDto[]> {
    return getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(ProjectContributorEntity, 'contributor');
      qb.leftJoinAndSelect('contributor.user', 'user');
      qb.leftJoinAndSelect('contributor.project', 'project');
      if (query && query.id) {
        qb.andWhere('contributor.id = :contributorId', { contributorId: query.id });
      }
      qb.orderBy('contributor.id', 'DESC');
      const data = await this.apiService.setPagination<ProjectContributorEntity>(qb);
      return data.map(value => new ProjectContributorDto(value));
    });
  }

  public async save(contributor: ProjectContributorEntity, authorId): Promise<ProjectContributorEntity> {
    if (contributor.id !== authorId) {
      await this.notificationService.addedProjectContributor(contributor.id, NotificationEventEnum.ADD_PROJECT_CONTRIBUTOR, contributor.project);
    }
    return this.projectContributorRepository.save(contributor);
  }
}
