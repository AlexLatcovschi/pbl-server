import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { OPTIONAL_AUTH_GUARD } from '../../auth/auth.constants';
import { SearchUserDto } from '../profile/dto/SearchUser.dto';
import { UserQueryDto } from '../query/userQueryDto';
import { ProjectsService } from '../projects/projects.service';
import { ProjectQueryDto } from '../query/projectQueryDto';
import { ProjectDto } from '../projects/dto/Project.dto';

@ApiUseTags('User')
@ApiBearerAuth()
@UseGuards(OPTIONAL_AUTH_GUARD)
@Controller('users')
export class UsersController {
  constructor(
    private readonly  userService: UsersService,
    private readonly projectService: ProjectsService,
  ) {
  }

  @Get()
  public findUsers(@Query() query: UserQueryDto): Promise<SearchUserDto[]> {
    return this.userService.search(query);
  }

  @Get(':id')
  public finUser(@Param('id') userId: number): Promise<SearchUserDto[]> {
    const query = new UserQueryDto();
    query.id = userId;
    return this.userService.search(query);
  }

  @Get(':id/projects')
  public getProjects(@Param('id') userId: number): Promise<ProjectDto[]> {
    const query = new ProjectQueryDto();
    query.authorId = userId;
    return this.projectService.search(query);
  }
}
