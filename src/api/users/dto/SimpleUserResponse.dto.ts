import { ApiResponseModelProperty } from '@nestjs/swagger';
import { UserEntity } from '../../entities/user/User.entity';

export class SimpleUserResponseDto {
  @ApiResponseModelProperty()
  public id?: number;
  @ApiResponseModelProperty()
  public firstName?: string;
  @ApiResponseModelProperty()
  public lastName?: string;
  @ApiResponseModelProperty()
  public pictureUrl?: string;

  constructor(user: UserEntity) {
    this.id = user.id;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.pictureUrl = user.pictureUrl;
  }
}
