import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiService } from '../../utils/ApiService';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectContributorRepository } from '../repositories/ProjectContributorRepository';
import { ProjectEntity } from '../entities/project/Project.entity';
import { UserEntity } from '../entities/user/User.entity';
import { ProjectsModule } from '../projects/projects.module';
import { ContributionEntity } from '../entities/contribution/Contribution.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProjectContributorRepository, ContributionEntity, ProjectEntity, UserEntity]),
    ProjectsModule,
  ],
  providers: [UsersService, ApiService],
  exports: [UsersService],
  controllers: [UsersController],

})
export class UsersModule {
}
