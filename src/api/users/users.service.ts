import { Injectable } from '@nestjs/common';
import { Brackets, EntityManager, getManager, WhereExpression } from 'typeorm';
import { UserEntity } from '../entities/user/User.entity';
import { ApiService } from '../../utils/ApiService';
import { SearchUserDto } from '../profile/dto/SearchUser.dto';
import { UserQueryDto } from '../query/userQueryDto';

@Injectable()
export class UsersService {
  constructor(
    private readonly apiService: ApiService,
  ) {
  }

  public async search(query?: UserQueryDto): Promise<SearchUserDto[]> {
    return getManager().transaction(async entityManager => {
      const qb = await this.getBaseUserQB(entityManager);
      if (query && query.id) {
        qb.andWhere('user.id = :userId', { userId: query.id });
      }
      if (query && query.search) {
        const formattedQuery = `%${query.search.toLowerCase()}%`;
        qb.setParameter('formattedQuery', formattedQuery);
        if (query.projectId) {
          qb.andWhere(sqb => {
            const subQuery = sqb
              .subQuery()
              .from(UserEntity, 'user2')
              .leftJoin('user2.contributors', 'contributors')
              .leftJoin('contributors.project', 'projects')
              .select('projects.id')
              .where('user.id = user2.id')
              .getQuery();
            return query.projectId + ' NOT IN ' + subQuery;
          });
        }
        qb.andWhere(
          new Brackets(
            (subQb: WhereExpression): any => {
              subQb.orWhere(`LOWER(user.firstName) like :formattedQuery`);
              subQb.orWhere(`LOWER(user.lastName) like :formattedQuery`);
            },
          ),
        );
      }
      const data = await this.apiService.setPagination<UserEntity>(qb);
      return data.map(value => new SearchUserDto(value));
    });
  }

  public async getUserById(id): Promise<UserEntity> {
    return await getManager().transaction(async entityManager => {
      const qb = entityManager.createQueryBuilder(UserEntity, 'alias');
      qb.where('alias.id = :userId', { userId: id });
      return await this.apiService.getOne(qb);
    });
  }

  private getBaseUserQB(entityManager: EntityManager) {
    const qb = entityManager.createQueryBuilder(UserEntity, 'user');
    qb.select(['user.id', 'user.firstName', 'user.lastName', 'user.pictureUrl']);
    return qb;
  }
}
