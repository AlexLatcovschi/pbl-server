import * as jwt from 'jsonwebtoken';
import { AUTHORITY } from './app.constants';

export interface ITokenUser {
  id: number;
  authorities: AUTHORITY[];
}

export const generateValidationKey = (): string => {
  return String(Math.floor(10000000000000000000 + Math.random() * 90000000000000000000));
};

export const getUserFormAuthorizationHeader = (token: string): ITokenUser => {
  return jwt.decode(token) as ITokenUser;
};
