import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '../config/config.service';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        type: config.get('TYPEORM_CONNECTION') as any,
        host: config.get('TYPEORM_HOST'),
        port: +config.get('TYPEORM_PORT'),
        username: config.get('TYPEORM_USERNAME'),
        password: config.get('TYPEORM_PASSWORD'),
        database: config.get('TYPEORM_DATABASE'),
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
        synchronize: Boolean(config.get('TYPEORM_SYNCHRONIZE')),
        logging: config.get('TYPEORM_LOGGING') as any,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {
}
