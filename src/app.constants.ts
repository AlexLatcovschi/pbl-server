export const HTTP_MESSAGES = {
  UNAUTHORIZED_INVALID_EMAIL: 'EmailNotExists',
  UNAUTHORIZED_INVALID_PASSWORD: 'InvalidPassword',
  UNAUTHORIZED_INVALID_ID: 'IdNotExists',
  UNAUTHORIZED_NOT_ACTIVATED: 'EmailNotActivated.',
};

export enum AUTHORITY {
  USER = 'USER',
  BLOCKED = 'BLOCKED',
  ROLE_ADMIN = 'ROLE_ADMIN',
}

export enum HTTP_STATUS {
  OK = 200,
  CREATED = 201,
  UPDATED = 202,
  DELETED = 204,
}

export enum MULTIPLIER {
  MONEY = 4,
  TIME = 2,
}
