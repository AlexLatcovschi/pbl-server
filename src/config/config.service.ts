import * as dotenv from 'dotenv';
import * as fs from 'fs';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
      this.envConfig = {
        APP_NAME: 'Campanii TV8',
        NODE_ENV: process.env.NODE_ENV,
        API_FULL_HOST: process.env.API_FULL_HOST,
        CLIENT_FULL_HOST: process.env.CLIENT_FULL_HOST,
        TYPEORM_CONNECTION: process.env.TYPEORM_CONNECTION,
        TYPEORM_HOST: process.env.TYPEORM_HOST,
        TYPEORM_USERNAME: process.env.TYPEORM_USERNAME,
        TYPEORM_PASSWORD: process.env.TYPEORM_PASSWORD,
        TYPEORM_DATABASE: process.env.TYPEORM_DATABASE,
        TYPEORM_PORT: process.env.TYPEORM_PORT,
        TYPEORM_SYNCHRONIZE: process.env.TYPEORM_SYNCHRONIZE,
        TYPEORM_LOGGING: process.env.TYPEORM_LOGGING,
        JWT_SECRET: process.env.JWT_SECRET,
        JWT_EXPIRE: process.env.JWT_EXPIRE,
        SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
        FACEBOOK_ID: process.env.FACEBOOK_ID,
        FACEBOOK_SECRET: process.env.FACEBOOK_SECRET,
        GOOGLE_ID: process.env.GOOGLE_ID,
        GOOGLE_SECRET: process.env.GOOGLE_SECRET,
      };
    } else {
      this.envConfig = dotenv.parse(fs.readFileSync('.env'));
    }
  }

  public get(key: string): string {
    return this.envConfig[key];
  }
}
