import { createParamDecorator } from '@nestjs/common';
import { Request } from 'express';
import { getUserFormAuthorizationHeader } from '../utils';

export const AuthUser = createParamDecorator((data, req) => {
  // tslint:disable-next-line:no-console
  return req.user;
});

export const CurrentTokenUser = createParamDecorator((data, req: Request) => {
  const authorizationHeader = req.header('Authorization');
  // tslint:disable-next-line:no-console
  if (authorizationHeader) {
    return getUserFormAuthorizationHeader(authorizationHeader.substring(7));
  }
  return undefined;
});
