import { SetMetadata } from '@nestjs/common';
import { AUTHORITY } from '../app.constants';

export const Roles = (...roles: AUTHORITY[]) => SetMetadata('authorities', roles);
