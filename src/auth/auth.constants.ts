import { AuthGuard } from '@nestjs/passport';
import { OptionalJwtAuth } from './extensions/OptionalJwtAuth';
import { AdminGuard } from './extensions/AdminGuard';

export const AUTH_GUARD = AuthGuard('jwt');
export const OPTIONAL_AUTH_GUARD = OptionalJwtAuth;
export const ADMIN_GUARD = AdminGuard;
