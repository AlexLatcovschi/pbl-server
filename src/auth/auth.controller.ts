import { Body, Controller, Get, Post, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthUser } from '../decorators/user.decorator';
import { UserEntity } from '../api/entities/user/User.entity';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { ConfigService } from '../config/config.service';
import { AuthResponseDto } from './dto/AuthTokenDto';
import { LoginDto } from './dto/LoginDto';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private readonly config: ConfigService) {
  }

  @Post('local')
  public async local(@Body() { email, password }: LoginDto): Promise<AuthResponseDto> {
    const authUser = await this.authService.validateLocalLogin(email, password);
    const token = this.authService.createToken(authUser);
    return new AuthResponseDto(token);
  }

  @Get('google')
  @UseGuards(AuthGuard('google'))
  public google(): any {
    return;
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  public googleCallback(@AuthUser() authUser: UserEntity, @Res() res: Response): void {
    this.processOAuthLoginCallback(authUser, res);
  }

  @Get('facebook')
  @UseGuards(AuthGuard('facebook'))
  public facebook(): any {
    return;
  }

  @Get('facebook/callback')
  @UseGuards(AuthGuard('facebook'))
  public facebookCallback(@AuthUser() authUser: UserEntity, @Res() res: Response): void {
    this.processOAuthLoginCallback(authUser, res);
  }

  private processOAuthLoginCallback(user: UserEntity, res: Response): void {
    res.redirect(`${this.config.get('CLIENT_FULL_HOST')}/login?token=${this.authService.createToken(user)}`);
  }
}
