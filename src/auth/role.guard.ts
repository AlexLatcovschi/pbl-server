import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { getUserFormAuthorizationHeader, ITokenUser } from '../utils';
import { AUTHORITY } from '../app.constants';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {
  }

  public canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<AUTHORITY[]>('authorities', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const authorizationHeader = request.header('Authorization');
    const user: ITokenUser = getUserFormAuthorizationHeader(authorizationHeader.substring(7));
    const hasRole = () => user.authorities.some(role => roles.includes(role));
    return user && user.authorities && hasRole();
  }
}
