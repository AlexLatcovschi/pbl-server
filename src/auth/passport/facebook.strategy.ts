import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from '../auth.service';
import { AuthProviderEnum } from '../../api/enum/AuthProvider.enum';
import { ConfigService } from '../../config/config.service';
import { Strategy } from 'passport-strategy';

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
  constructor(private readonly authService: AuthService, private readonly config: ConfigService) {
    super({
      clientID: config.get('FACEBOOK_ID'),
      clientSecret: config.get('FACEBOOK_SECRET'),
      callbackURL: config.get('API_FULL_HOST') + '/api/auth/facebook/callback',
      scope: ['email'],
      profileFields: ['name', 'email', 'link', 'locale', 'timezone', 'gender', 'verified'],
      passReqToCallback: true,
    });
  }

  async validate(request: any, accessToken: string, refreshToken: string, profile, done): Promise<void> {
    await this.authService
      .validateOAuthLogin(profile, AuthProviderEnum.FACEBOOK)
      .then(user => done(null, user))
      .catch(err => done(err, false));
  }
}
