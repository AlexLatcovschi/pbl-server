import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { AuthService } from '../auth.service';
import { AuthProviderEnum } from '../../api/enum/AuthProvider.enum';
import { ConfigService } from '../../config/config.service';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private readonly authService: AuthService, private readonly config: ConfigService) {
    super({
      clientID: config.get('GOOGLE_ID'),
      clientSecret: config.get('GOOGLE_SECRET'),
      callbackURL: config.get('API_FULL_HOST') + '/api/auth/google/callback',
      scope: ['profile'],
      passReqToCallback: true,
    });
  }

  async validate(request: any, accessToken: string, refreshToken: string, profile, done) {
    await this.authService
      .validateOAuthLogin(profile, AuthProviderEnum.GOOGLE)
      .then(user => done(null, user))
      .catch(err => done(err, false));
  }
}
