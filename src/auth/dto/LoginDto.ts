import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiModelProperty()
  @IsEmail()
  public readonly email: string;
  @ApiModelProperty()
  @IsNotEmpty()
  public readonly password: string;
}
