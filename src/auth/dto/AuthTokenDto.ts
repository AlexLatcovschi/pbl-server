export class AuthResponseDto {
  public static expire = process.env.JWT_EXPIRE;
  public token: string;

  constructor(token: string) {
    this.token = token;
  }
}
