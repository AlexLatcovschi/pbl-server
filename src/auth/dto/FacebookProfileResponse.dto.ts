export interface Name {
  familyName: string;
  givenName: string;
}

export interface Email {
  value: string;
}

export interface Json {
  last_name: string;
  first_name: string;
  email: string;
  id: string;
}

export interface FacebookProfileResponseDto {
  id: string;
  name: Name;
  emails: Email[];
  provider: string;
  _raw: string;
  _json: Json;
}
