import { AUTHORITY } from '../../app.constants';

export const ROLE_ADMIN = 'ROLE_ADMIN';

export interface UserJwt {
  id: number;
  authorities: AUTHORITY[];
  roles?: string[];
}

export const isAdmin = (userJwt: UserJwt): boolean => {
  return userJwt && userJwt.roles.includes(ROLE_ADMIN);
};
