import { UserJwt } from './UserJwt';

export type PayloadJwt = UserJwt & { iat: number; exp: number };
