import { AuthGuard } from '@nestjs/passport';

export class AdminGuard extends AuthGuard('jwt') {
  handleRequest(err, user, info, context) {
    return user;
  }
}
