import { AuthGuard } from '@nestjs/passport';

export class OptionalJwtAuth extends AuthGuard('jwt') {
  handleRequest(err, user, info, context) {
    return user;
  }
}
