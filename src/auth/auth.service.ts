import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserEntity } from '../api/entities/user/User.entity';
import { JwtService } from '@nestjs/jwt';
import { UserJwt } from './dto/UserJwt';
import { HTTP_MESSAGES } from '../app.constants';
import * as bcrypt from 'bcrypt';
import { getManager } from 'typeorm';
import { AuthProviderEnum } from '../api/enum/AuthProvider.enum';
import { PayloadJwt } from './dto/PayloadJwt';
import { FacebookProfileResponseDto } from './dto/FacebookProfileResponse.dto';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {
  }

  public createToken(user: UserEntity): string {
    return this.jwtService.sign(this.formatJwtPayload(user));
  }

  public async validateLocalLogin(email: string, password: string): Promise<UserEntity> {
    const user = await getManager()
      .createQueryBuilder(UserEntity, 'user')
      .addSelect('user.password')
      .where('user.email = :email', { email })
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('roles.authorities', 'authorities')
      .getOne();
    if (!user) {
      throw new UnauthorizedException(HTTP_MESSAGES.UNAUTHORIZED_INVALID_EMAIL);
    }

    if (!bcrypt.compareSync(password, user.password)) {
      throw new UnauthorizedException(HTTP_MESSAGES.UNAUTHORIZED_INVALID_PASSWORD);
    }

    return user;
  }

  public async validateJwtLogin(payload: PayloadJwt): Promise<UserJwt> {
    return payload;
  }

  public async validateOAuthLogin(
    profile: FacebookProfileResponseDto | any,
    provider: AuthProviderEnum,
  ): Promise<UserEntity> {
    return getManager().transaction(async entityManager => {
      const qb = entityManager
        .createQueryBuilder(UserEntity, 'user')
        .where('user.thirdPartyId = :thirdPartyId', { thirdPartyId: profile.id })
        .andWhere('user.provider = :provider', { provider })

        .leftJoinAndSelect('user.roles', 'roles')
        .leftJoinAndSelect('roles.authorities', 'authorities');
      if (profile._json.email) {
        qb.orWhere('user.email = :email', { email: profile._json.email });
      }
      let user = await qb.getOne();
      if (!user) {
        user = new UserEntity();
        user.provider = provider;
        user.thirdPartyId = profile.id;
        user.firstName = user.firstName || profile.name.givenName;
        user.lastName = user.lastName || profile.name.familyName;
        user.email = user.email || profile._json.email;
        await entityManager.save(UserEntity, user);
      } else {
        let updated = false;
        if (!user.email) {
          user.email = profile._json.email;
          updated = true;
        }
        if (!user.provider) {
          user.provider = provider;
          updated = true;
        }
        if (!user.thirdPartyId) {
          user.thirdPartyId = profile.id;
          updated = true;
        }
        if (updated) {
          await entityManager.save(UserEntity, user);
        }
      }
      return user;
    });
  }

  private formatJwtPayload(user: UserEntity): UserJwt {
    const authorities = [];
    if (user.roles) {
      for (const role of user.roles) {
        if (role.authorities) {
          const roleAuthorities = role.authorities.map(value => value.name);
          for (const roleAuthority of roleAuthorities) {
            if (!authorities.includes(roleAuthority)) {
              authorities.push(roleAuthority);
            }
          }
        } else {
          if (!authorities.includes(role.name)) {
            authorities.push(role.name);
          }
        }
      }
    }
    return { id: user.id, authorities, roles: user.roles ? user.roles.map(value => value.name) : [] };
  }
}
