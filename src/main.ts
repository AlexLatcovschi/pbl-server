import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap(): Promise<any> {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.use(helmet());
  app.enableCors();
  app.setGlobalPrefix('api');

  app.use('/api/pub/', rateLimit({ windowMs: 30 * 60 * 1000, /* 30 minutes*/ max: 10 }));

  SwaggerModule.setup(
    'swagger',
    app,
    SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle('divysion')
        .setDescription('Divysion API')
        .setVersion('1.0')
        .setBasePath('api')
        .setSchemes('http')
        .addBearerAuth()
        .build(),
    ),
  );

  await app.listen(3000);
}

bootstrap();
