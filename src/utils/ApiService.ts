import { Inject, Injectable, Scope } from '@nestjs/common';
import { Request } from 'express';
import { SelectQueryBuilder } from 'typeorm';
import { REQUEST } from '@nestjs/core';

@Injectable({ scope: Scope.REQUEST })
export class ApiService {
  constructor(@Inject(REQUEST) public request: Request) {
  }

  public async setPagination<T>(qb: SelectQueryBuilder<T>): Promise<T[]> {
    this.request.res.append('Access-Control-Expose-Headers', 'X-Total-Count');
    const take = this.formatResponseSize(this.request);
    qb.take(take);
    qb.skip(take * (this.request.query.page || 0));
    const [data, count] = await qb.getManyAndCount();
    this.request.res.append('X-Total-Count', String(count));
    return data;
  }

  public async getOne<T>(qb: SelectQueryBuilder<T>): Promise<T> {
    const take = this.formatResponseSize(this.request);
    qb.take(take);
    const data = await qb.getOne();
    return data;
  }
  public async getMany<T>(qb: SelectQueryBuilder<T>): Promise<T[]> {
    return await qb.getMany();;
  }

  private formatResponseSize(req: Request): number {
    return +req.query.size ? (req.query.size > 100 ? 100 : req.query.size) : 10;
  }
}
