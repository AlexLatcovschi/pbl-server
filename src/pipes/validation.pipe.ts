import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  public async transform(value: any, { metatype }: ArgumentMetadata): Promise<any> {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object, { validationError: { target: false, value: false } });
    if (errors.length > 0) {
      throw new BadRequestException(
        errors.reduce((obj, cur) => {
          return {
            ...obj,
            [cur.property]: Object.keys(cur.constraints).map(value1 => {
              return { [value1]: cur.constraints[value1] };
            }),
          };
        }, {}),
      );
    }
    return value;
  }

  private toValidate(metatype: any): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find(type => metatype === type);
  }
}
